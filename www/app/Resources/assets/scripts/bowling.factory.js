///**
// * @author    Jeroen Knockaert
// * @copyright Copyright © 2015-2016 Artevelde University College Ghent
// * @license   Apache License, Version 2.0
// */
//;(function () { 'use strict';
//
//    angular.module('BowleeApp')
//        .factory('BowlingFactory', BowlingFactory);
//
//    BowlingFactory.$inject = [
//        '$http',
//        '$state'
//    ];
//
//    function BowlingFactory(
//
//        $http,
//        $state
//    ) {
//
//        return {
//
//            createBowling : function(CreateBowling) {
//                console.log('BowlingFactory.createBowling');
//                $http.post('http://www.bachelor.proef/app_dev.php/bowling.json',
//                    {
//                        'airline'   : CreateBowling.airline,
//                        'number'    : CreateBowling.number,
//                        'day'       : CreateBowling.day
//                    })
//                    .then (function successCallback (data, status, headers, config){
//                            console.log ("data sent to API, new object created");
//                            $state.go('/bowling',  {reload:true});
//                        },
//                        function errorCallback (){
//                            console.log("data not sent to API, new object is not created");
//                        });
//            },
//
//            getAllBowlings : function(GetAllBowlings) {
//                $http.get('http://www.bachelor.proef/app_dev.php/bowling.json')
//                    .then (function successCallback (response){
//                            GetAllBowlings.bowling = response.data;
//                            console.log("GetAllBowlings works!");
//                            console.log(GetAllBowlings.bowling);
//                        },
//                        function errorCallback (){
//                            console.log("GetAllBowlings doesn't works!");
//                        });
//            }
//        }
//    }
//})();