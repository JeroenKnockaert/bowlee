angular.module('BowleeApp', ["ngResource"])
  .controller('MainCtrl', function ($scope, $http) {

    // Accepts form input
    $scope.submit = function() {

        // POSTS data to webservice
        postName($scope.input);

        // GET data from webservice
        var name = getName();

        // DEBUG: Construct greeting
        $scope.greeting = 'Sup ' + name + ' !';

    };

    function postName (dataToPost) {

      $http.post('/name', dataToPost).
      success(function(data) {
        $scope.error = false;
        $scope.data = data;
      }).
      error(function(data) {
        $scope.error = true;
        $scope.data = data;
      });
    }

    // GET name from webservice
    function getName () {

        $http.get('/name').
        success(function(data) {
          $scope.error = false;
          $scope.data = data;

          return data;
        }).
        error(function(data) {
          $scope.error = true;
          $scope.data = data;

          return 'error name'; 
        });

    } 
})