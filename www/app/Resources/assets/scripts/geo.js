/**
 * @author    Jeroen Knockaert
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 *
 * File: geo.js
 */


function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);

    } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
  var lat = position.coords.latitude;
  var lng = position.coords.longitude;

    x.innerHTML = "Latitude: " + lat + "<br>Longitude: " + lng;
    var results = getDetails(lat, lng)
    //console.log(results);
}

function getDetails(lat, long){
    var request = new XMLHttpRequest();

    var method = 'GET';
    var url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + long + '&sensor=true';
    var async = true;

    request.open(method, url, async);
    request.onreadystatechange = function(){
        if(request.readyState == 4 && request.status == 200){
            
            var data = JSON.parse(request.responseText);
            var address = data.results[0];
            console.log(address);
            var location = {
              "street_number": address['address_components'][0]['long_name'],
              "route": address['address_components'][1]['long_name'],
              "city": address['address_components'][2]['long_name'],
              "county": address['address_components'][3]['long_name'],
              "region": address['address_components'][4]['long_name'],
              "country": address['address_components'][5]['long_name'],
              "postal_code": address['address_components'][6]['long_name'],
            };
            //x.innerHTML = "Latituddddddde: "  + "<br>Longitude: " ;
            //x.innerHTML .= "Latituddddddde: "  + "<br>Longitude: " ;
            //console.log(location.size());
            var message = "<pre>";
            for (var key in location) {
              //message += "key " + key + " has value " + location[key];
              message += "<p> " + key + " : " + location[key]+ "</p>";
            }
            message += "</pre>";
            x.innerHTML = message;
                // var str = '<ul>';
                // for (var x = 0; x < location.length; x++) {
                //     str += '<li>' + location[x] + '</li>';
                // }
                // str += '</ul>';
                // $('#demo').innerHTML = str;

            console.log(location);
            var locationStreetNumber = address['address_components'][0]['long_name'];
            var locationRoute = address['address_components'][1]['long_name'];
            var locationCity = address['address_components'][2]['long_name'];
            var locationCounty = address['address_components'][3]['long_name'];
            var locationRegion = address['address_components'][4]['long_name'];
            var locationCountry = address['address_components'][5]['long_name'];
            var locationPostalCode = address['address_components'][6]['long_name'];
           


/*        $scope.City = pictureCity;
        $scope.PostalCode = parseInt(picturePostalCode);
        $scope.Country = pictureCountry;*/
        console.log('extra info: City: ' + locationCity + ', Postcode: ' + locationPostalCode + ', Country: ' + locationCountry);
        }
    };
    request.send();
}
