//form.js

$(function() {
    function displayResult(item) {
        $('.alert').show().html('You selected <strong>' + item.value + '</strong>: <strong>' + item.text + '</strong>');
    }
        //serie get current id
        var currentType = $(".form-title h2").attr('data-id');
        $("#appbundle_seriefront_new_type").html('<option value="' + currentType + '" selected="selected"></option>"');

        var currentSerie = $(".form-title .row h1").attr('data-serie-id');
        $("#appbundle_game_new_serie").html('<option value="' + currentSerie + '" selected="selected">' + currentSerie + '</option>"');

        //serie type id
        var currentSerieType = $(".form-title .row h2").attr('data-id');
        $("#appbundle_game_new_type").html('<option value="' + currentSerieType + '" selected="selected">' + currentSerieType + '</option>"');
        
        var currentBowling = $(".form-title h3").attr('data-bowling');
        $("#appbundle_game_new_bowling").html('<option value="' + currentBowling + '" selected="selected"></option>"');
    function getBowlings(){
 
        $.get( "../../api/v1/bowling.json?sort=id&order=asc", function( data ) {

            var obj = {
                source:[],
                //onSelect: displayResult
            };
            var len = data.bowlingcentra.length;
            for (var i = 0; i < len; i++) {
                obj['source'].push({
                    id: data.bowlingcentra[i].id,
                    name: data.bowlingcentra[i].name,
                    resizeable: true
                });
            }
//            console.log('arr', obj);
//            console.log(data);
//            console.log(data.bowlingcentra.length);
//            alert( "Load was performed." );
//            console.log('getobj', obj);

            function loadBowlings(object){
                $('#demo1').typeahead(object);
            }

            loadBowlings(obj);

        });


    }

    getBowlings();

    $('#demo1').focus(function(){
        var selectedBowling = $(".typeahead li.active").attr('data-value');
        var selectedBowlingName = $(".typeahead li.active a").text();
        $("#appbundle_seriefront_new_bowling").html('<option value="' + selectedBowling + '" selected="selected">' + selectedBowlingName + '</option>"');

    });
    
    $('.btn-game').click(function(){
        console.log('yay');
    });
});