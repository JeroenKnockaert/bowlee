/**
 * Custom Gulp Tasks.
 * 
 * @author    Jeroen Knockaert
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
	
	
	// grab our gulp packages
	var gulp = require('gulp');
	var gutil = require('gulp-util');
	var concat = require('gulp-concat');
	var sass = require('gulp-sass');
	var minifyCss = require('gulp-minify-css');
	var rename = require('gulp-rename');
	var sh = require('shelljs');



	var paths = {
		sass: ['./assets/sass/*.scss'],
		scripts :['./assets/scripts/*.js']
	};
	

	// create a default task and just log a message
	gulp.task('default', function() {
	  return gutil.log('Gulp is running!')
	});
	
	
		gulp.watch(paths.sass, ['sass']);
		gulp.watch(paths.scripts, ['scripts']);
		//gulp.watch('source/**/*.html', ['copyHtml']);
	
	// Concatenate all scripts to `./www/js/app.js`.
	gulp.task('scripts', scripts);
	function scripts() {
		return gulp
			.src('./assets/scripts/*.js')
			.pipe(concat('app.js'))
			.pipe(gulp.dest('../../web/assets/js'));
	}
	
	
	gulp.task('default', ['sass']);

	gulp.task('sass', function(done) {
	gulp.src('./assets/sass/front.scss')
		.pipe(sass({
		errLogToConsole: true
		}))
		.pipe(gulp.dest('../../web/assets/css/'))
		.pipe(minifyCss({
		keepSpecialComments: 0
		}))
		.pipe(rename({ extname: '.min.css' }))
		.pipe(gulp.dest('../../web/assets/css/'))
		.on('end', done);
	});
	