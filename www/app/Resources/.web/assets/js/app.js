angular.module('app')
    .controller('MainCtrl', ['$scope', '$route', 'Post',
        function($scope, $route, Post) {
            $scope.post = new Post();
            $scope.posts = Post.query();
            
            $scope.save = function() {
                $scope.post.$save();
                $scope.post.push($scope.post);
                $scope.post = new Post();
            }
            
            $scope.delete = function() {
                Post.delete(post);
                _.remove($scope.posts, post);
            }
        }
    ]);

(function(){

    google.maps.event.addDomListener(window, 'load', init);
    var allCoords = JSON.parse(document.getElementById('map').getAttribute('data-coords'));

    function init() {
        var mapOptions = {
            zoom: 6,
            center: new google.maps.LatLng(51.054342, 3.717424), // Gent
            styles: [{
                "featureType": "landscape",
                "stylers": [{"hue": "#FFA800"}, {"saturation": 0}, {"lightness": 0}, {"gamma": 1}]
            }, {
                "featureType": "road.highway",
                "stylers": [{"hue": "#53FF00"}, {"saturation": -73}, {"lightness": 40}, {"gamma": 1}]
            }, {
                "featureType": "road.arterial",
                "stylers": [{"hue": "#FBFF00"}, {"saturation": 0}, {"lightness": 0}, {"gamma": 1}]
            }, {
                "featureType": "road.local",
                "stylers": [{"hue": "#00FFFD"}, {"saturation": 0}, {"lightness": 30}, {"gamma": 1}]
            }, {
                "featureType": "water",
                "stylers": [{"hue": "#00BFFF"}, {"saturation": 6}, {"lightness": 8}, {"gamma": 1}]
            }, {
                "featureType": "poi",
                "stylers": [{"hue": "#679714"}, {"saturation": 33.4}, {"lightness": -25.4}, {"gamma": 1}]
            }]

        };
        var mapElement = document.getElementById('map');
        var map = new google.maps.Map(mapElement, mapOptions);

        //Place all the Markers
        for (var i = 0; i < allCoords.length; i++) {

            var picture = new google.maps.Marker({
                position: new google.maps.LatLng(allCoords[i].lat, allCoords[i].lng),
                map: map,
                id: allCoords[i].id
            });

            linkToPicture(picture);
        }
    }

    function linkToPicture(picture) {

        google.maps.event.addListener(picture, 'click', function() {
            var currentUrl = window.location.host;
            var urlPath = "/app_dev.php";
            window.location =  urlPath + "Admin/Bowlingcentra/" + picture.id;
        });
    }

})();