///**
// * @author    Jeroen Knockaert
// * @copyright Copyright © 2015-2016 Artevelde University College Ghent
// * @license   Apache License, Version 2.0
// */
//;(function () { 'use strict';
//
//    angular.module('BowleeApp')
//        .factory('BowlingFactory', BowlingFactory);
//
//    BowlingFactory.$inject = [
//        '$http',
//        '$state'
//    ];
//
//    function BowlingFactory(
//
//        $http,
//        $state
//    ) {
//
//        return {
//
//            createBowling : function(CreateBowling) {
//                console.log('BowlingFactory.createBowling');
//                $http.post('http://www.bachelor.proef/app_dev.php/bowling.json',
//                    {
//                        'airline'   : CreateBowling.airline,
//                        'number'    : CreateBowling.number,
//                        'day'       : CreateBowling.day
//                    })
//                    .then (function successCallback (data, status, headers, config){
//                            console.log ("data sent to API, new object created");
//                            $state.go('/bowling',  {reload:true});
//                        },
//                        function errorCallback (){
//                            console.log("data not sent to API, new object is not created");
//                        });
//            },
//
//            getAllBowlings : function(GetAllBowlings) {
//                $http.get('http://www.bachelor.proef/app_dev.php/bowling.json')
//                    .then (function successCallback (response){
//                            GetAllBowlings.bowling = response.data;
//                            console.log("GetAllBowlings works!");
//                            console.log(GetAllBowlings.bowling);
//                        },
//                        function errorCallback (){
//                            console.log("GetAllBowlings doesn't works!");
//                        });
//            }
//        }
//    }
//})();
angular.module('BowleeApp', ["ngResource"])
  .controller('MainCtrl', function ($scope, $http) {

    // Accepts form input
    $scope.submit = function() {

        // POSTS data to webservice
        postName($scope.input);

        // GET data from webservice
        var name = getName();

        // DEBUG: Construct greeting
        $scope.greeting = 'Sup ' + name + ' !';

    };

    function postName (dataToPost) {

      $http.post('/name', dataToPost).
      success(function(data) {
        $scope.error = false;
        $scope.data = data;
      }).
      error(function(data) {
        $scope.error = true;
        $scope.data = data;
      });
    }

    // GET name from webservice
    function getName () {

        $http.get('/name').
        success(function(data) {
          $scope.error = false;
          $scope.data = data;

          return data;
        }).
        error(function(data) {
          $scope.error = true;
          $scope.data = data;

          return 'error name'; 
        });

    } 
})
//form.js

$(function() {
    function displayResult(item) {
        $('.alert').show().html('You selected <strong>' + item.value + '</strong>: <strong>' + item.text + '</strong>');
    }
        //serie get current id
        var currentType = $(".form-title h2").attr('data-id');
        $("#appbundle_seriefront_new_type").html('<option value="' + currentType + '" selected="selected"></option>"');

        var currentSerie = $(".form-title .row h1").attr('data-serie-id');
        $("#appbundle_game_new_serie").html('<option value="' + currentSerie + '" selected="selected">' + currentSerie + '</option>"');

        //serie type id
        var currentSerieType = $(".form-title .row h2").attr('data-id');
        $("#appbundle_game_new_type").html('<option value="' + currentSerieType + '" selected="selected">' + currentSerieType + '</option>"');
        
        var currentBowling = $(".form-title h3").attr('data-bowling');
        $("#appbundle_game_new_bowling").html('<option value="' + currentBowling + '" selected="selected"></option>"');
    function getBowlings(){
 
        $.get( "../../api/v1/bowling.json?sort=id&order=asc", function( data ) {

            var obj = {
                source:[],
                //onSelect: displayResult
            };
            var len = data.bowlingcentra.length;
            for (var i = 0; i < len; i++) {
                obj['source'].push({
                    id: data.bowlingcentra[i].id,
                    name: data.bowlingcentra[i].name,
                    resizeable: true
                });
            }
//            console.log('arr', obj);
//            console.log(data);
//            console.log(data.bowlingcentra.length);
//            alert( "Load was performed." );
//            console.log('getobj', obj);

            function loadBowlings(object){
                $('#demo1').typeahead(object);
            }

            loadBowlings(obj);

        });


    }

    getBowlings();

    $('#demo1').focus(function(){
        var selectedBowling = $(".typeahead li.active").attr('data-value');
        var selectedBowlingName = $(".typeahead li.active a").text();
        $("#appbundle_seriefront_new_bowling").html('<option value="' + selectedBowling + '" selected="selected">' + selectedBowlingName + '</option>"');

    });
    
    $('.btn-game').click(function(){
        console.log('yay');
    });
});
/**
 * @author    Jeroen Knockaert
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 *
 * File: geo.js
 */


function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);

    } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
  var lat = position.coords.latitude;
  var lng = position.coords.longitude;

    x.innerHTML = "Latitude: " + lat + "<br>Longitude: " + lng;
    var results = getDetails(lat, lng)
    //console.log(results);
}

function getDetails(lat, long){
    var request = new XMLHttpRequest();

    var method = 'GET';
    var url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + long + '&sensor=true';
    var async = true;

    request.open(method, url, async);
    request.onreadystatechange = function(){
        if(request.readyState == 4 && request.status == 200){
            
            var data = JSON.parse(request.responseText);
            var address = data.results[0];
            console.log(address);
            var location = {
              "street_number": address['address_components'][0]['long_name'],
              "route": address['address_components'][1]['long_name'],
              "city": address['address_components'][2]['long_name'],
              "county": address['address_components'][3]['long_name'],
              "region": address['address_components'][4]['long_name'],
              "country": address['address_components'][5]['long_name'],
              "postal_code": address['address_components'][6]['long_name'],
            };
            //x.innerHTML = "Latituddddddde: "  + "<br>Longitude: " ;
            //x.innerHTML .= "Latituddddddde: "  + "<br>Longitude: " ;
            //console.log(location.size());
            var message = "<pre>";
            for (var key in location) {
              //message += "key " + key + " has value " + location[key];
              message += "<p> " + key + " : " + location[key]+ "</p>";
            }
            message += "</pre>";
            x.innerHTML = message;
                // var str = '<ul>';
                // for (var x = 0; x < location.length; x++) {
                //     str += '<li>' + location[x] + '</li>';
                // }
                // str += '</ul>';
                // $('#demo').innerHTML = str;

            console.log(location);
            var locationStreetNumber = address['address_components'][0]['long_name'];
            var locationRoute = address['address_components'][1]['long_name'];
            var locationCity = address['address_components'][2]['long_name'];
            var locationCounty = address['address_components'][3]['long_name'];
            var locationRegion = address['address_components'][4]['long_name'];
            var locationCountry = address['address_components'][5]['long_name'];
            var locationPostalCode = address['address_components'][6]['long_name'];
           


/*        $scope.City = pictureCity;
        $scope.PostalCode = parseInt(picturePostalCode);
        $scope.Country = pictureCountry;*/
        console.log('extra info: City: ' + locationCity + ', Postcode: ' + locationPostalCode + ', Country: ' + locationCountry);
        }
    };
    request.send();
}

/**
 * @author    Jeroen Knockaert
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 *
 * File: main.js
 */
 
//angular.module('app')
//    .controller('MainCtrl', ['$scope', '$route', 'Post',
//        function($scope, $route, Post) {
//            $scope.post = new Post();
//            $scope.posts = Post.query();
//            
//            $scope.save = function() {
//                $scope.post.$save();
//                $scope.post.push($scope.post);
//                $scope.post = new Post();
//            }
//            
//            $scope.delete = function() {
//                Post.delete(post);
//                _.remove($scope.posts, post);
//            }
//        }
//    ]);
 
//scripts.js

angular.module('BowleeApp', ["ngResource"])

    .run(function($rootScope) {
        $rootScope.lastframe = null;
    })

    .filter('reverse', function() {
        return function(items) {
            return items.slice().reverse();
        };
    })

    .filter('slice', function() {
      return function(arr, start, end) {
        return (arr || []).slice(start, end);
        };
    })


    .controller('GameCtrl', function($http, $scope, $rootScope){

        $scope.submitFrame = function(){
            postFrame();
        };
        
        $scope.submitFling = function(){
            postFling();
        };
        
        $scope.database = function(){
            
            console.log('frames:', $scope.frames);
            var currentGame = $(".breadcrumbs .game").attr('data-game');
           
           


            angular.forEach($scope.frames, function (frame) {
                //console.log(frame);

                postFrame(
                        $scope,
                        currentGame,        //current game id
                        frame,        //current frame number
                        frame.gameTotal     //frame total score
                )

            });
            
            var dataGame = {
                "game": {
                    "total": $scope.frames[8].gameTotal
                    //"total": $scope.frames[10].gameTotal
                }
            };
            
            $http({
                url: '/api/v1/games/' + currentGame + '.json',
                method: "PUT",
                data: dataGame
            })
            .then(function(response) {
                // success
                console.log('PUT request' + response);
                console.log(response);
            });
        };

        function postFrame($scope, game, frame, total) {
 
        var dataFrame = {
            "frame": {
                "game": game,
                "frame": frame.frame,
                "total": total
            }
        };
 
        $http({
                url: '/api/v1/games/1/frames.json',
                method: "POST",
                data: dataFrame
        })
        .then(function(response) {
            // success
            console.log('Frame ' + frame + ' werd succesvol opgeslagen als frame ' + response.data.frame.id);
            $scope.lastframe = response.data.frame.id;
            console.log('$scope.lastframe',$scope.lastframe);

            angular.forEach(frame.flings, function (fling) {
                console.log('fling', fling);

                //postFling($scope, frame_id, worp, one, two, three, four, five, six, seven, eight, nine, ten, score, split, foul, spare, strike)
                postFling(
                    $scope,
                    response.data.frame.id,    //current frame
                    fling.id,               //worp (1,2 or 3)
                    fling.pins[1],          //pin 1
                    fling.pins[2],          //pin 2
                    fling.pins[3],          //pin 3
                    fling.pins[4],          //pin 4
                    fling.pins[5],          //pin 5
                    fling.pins[6],          //pin 6
                    fling.pins[7],          //pin 7
                    fling.pins[8],          //pin 8
                    fling.pins[9],          //pin 9
                    fling.pins[10],         //pin 10
                    fling.total,            //total
                    false,                  //Split
                    false,                  //Foul
                    fling.spare,            //Spare
                    fling.strike            //Strike
                )
            });
        },
            function(response) { // optional
                    // failed
                    console.error('Frame ' + frame + ' werd NIET opgeslagen ' + response.data.frame.id);
                    $scope.lastframe = response.data.frame.id;
                    return $scope.lastframe;
            });

            return $scope.lastframe;
        }
 
        //hier moet nog een put komen

        function postFling($scope, frame, worp, one, two, three, four, five, six, seven, eight, nine, ten, score, split, foul, spare, strike) {

                var dataWorp = {
                        "fling": {
                            "frame": frame,
                            "worp": worp,
                            "one": one,
                            "two": two,
                            "three": three,
                            "four": four,
                            "five": five,
                            "six": six,
                            "seven": seven,
                            "eight": eight,
                            "nine": nine,
                            "ten": ten,
                            "score": score,
                            "isSplit": split,
                            "isFoul": foul,
                            "isSpare": spare,
                            "isStrike": strike
                        }
                    };

                $http({
                        url: '/api/v1/games/1/frames/1/fling/1.json',
                        method: "POST",
                        data: dataWorp
                })
                .then(function(response) {
                        // success
                        console.log('Fling ' + worp + ' van frame ' + frame + ' werd succesvol opgeslagen');
                        console.log('response',response);
                },
                function(response) { // optional
                        // failed
                        console.error('Fling ' + worp + ' van frame ' + frame + ' werd NIET opgeslagen');
                        console.log('response',response);
                });
        }
    $scope.pins = [
        { id: 1, name: 'pin-one' },
        { id: 2, name: 'pin-two' },
        { id: 3, name: 'pin-three' },
        { id: 4, name: 'pin-four' },
        { id: 5, name: 'pin-five' },
        { id: 6, name: 'pin-six' },
        { id: 7, name: 'pin-seven' },
        { id: 8, name: 'pin-eight' },
        { id: 9, name: 'pin-nine' },
        { id: 10, name: 'pin-ten' },
    ];

    $scope.getScore = function(){
        return $scope.score;
    };

    $scope.date = new Date();

    $scope.totalFling= 0;

    flings = []; //hier init we hem voor de eerste keer dus is hij leeg
    $scope.flingNumber = 1;
    $scope.frameNumber = 1;

    $scope.frames = [];

    $scope.isLastFrame = false;
    $scope.isStrike = false;
    $scope.isSpare = false;
    totalScore = 0;
    gameTotal = 0;

    //if pin is clicked
    $scope.getChanges = function(){

    }

    $scope.debug = function(){
        console.log('flings: ', flings);
        console.log('frames: ', $scope.frames);
    }

    function resetDisabledPins(){
        angular.forEach($scope.pins, function (pin) {
            pin.selected = false;
            var target = angular.element( document.querySelector('#checkbox' + [pin.id]));
            target.attr('disabled', false);
        });
    }

    function resetPins(){
        angular.forEach($scope.pins, function (pin) {
            pin.selected = false;
        });
        $scope.isStrike = false;
        $scope.isSpare = false;
    }


    $scope.setStrike = function(){

        angular.forEach($scope.pins, function (pin) {
            pin.selected = true;
            console.log('strike');
        });
        //setStrike();
        saveFling();
        //nextFrame();

    }

    $scope.setSpare = function(){

        angular.forEach($scope.pins, function (pin) {
            pin.selected = true;
            console.log('spare');
        });

      // $scope.isStrike = false;
      // $scope.isSpare = true;

      //setSpare();
      saveFling();
      //nextFrame();

  }

    function setStrike(){
        $scope.isStrike = true;
        $scope.isSpare = false;
    }

    function setSpare(){
        $scope.isStrike = false;
        $scope.isSpare = true;
    }
/*
    $scope.nextFrame = function() {

        if(checkLimit($scope.frameNumber, 11) === true){
            console.log('next');
            nextFrame();
        }
        else {
          console.log('lol wtf');
      }

  }*/

    function nextFrame() {

        console.log('test, ok');

        totalFling = 0;

        total = [];

        angular.forEach(flings, function(fling) {
            total[fling.id] = fling.total;

                // if(fling.id == 1){ totalFirst = fling.total; }
                // if(fling.id == 2){ totalSecond = fling.total; }
                // if(fling.id == 3){ totalThird = fling.total; }

            totalFling = totalFling + fling.total;

            $scope.total = total;
            console.log('fling.total', fling.total);
        });

        $scope.totalFling = totalFling;

        $scope.thisFling = {
            'first' : total[1],
            'second' : total[2],
            'third' : total[3] 
        };

        console.log('length',total.length, total[1], total[2], total[3]);

        console.log('score per fling', total);

        console.log(' b4 frames:',$scope.frames);

        dataFrame = {
            'frame': $scope.frameNumber,
            'flings': flings,
            'total': totalFling,
            'label': totalFling,
            'gameTotal': 0,
        };

        $scope.frames = $scope.frames.concat(dataFrame);

        console.log('aftr frames:',$scope.frames);

        flings = [];

        $scope.frameNumber++;

        //we zetten fling terug op 1
        $scope.flingNumber = 1;

        resetDisabledPins();

        console.log('Frames na beurt opslaan', $scope.frames[0].flings);

        currentFrame = $scope.frameNumber;

        console.log('currentFrame', currentFrame);

        spareFrame = currentFrame - 1;
        strikeFrame = currentFrame -2;
        finalScore = 0;

    console.log('length',$scope.frames.length);
    
    if($scope.frames[0]) { //dit moet 0 worden

        // start pas op einde van derde frame
        if($scope.frames.length >= 3) {
            bonus = 0;

            for (i = $scope.frames.length-1; i < $scope.frames.length; i++) {

                // controleer voor extra punten 2 frames terug
                if($scope.frames[i - 2].flings[0].strike){ //indien het een strike was

                    $scope.frames[i - 2].label = $scope.frames[i - 1].total + $scope.frames[i - 2].total;

                    if($scope.frames[i - 1].flings[0].strike)  {
                        $scope.frames[i - 2].label += $scope.frames[i].flings[0].total;
                    }

                    if($scope.frames.length == 3) {
                        console.info('==3');
                        $scope.frames[i-2].gameTotal = $scope.frames[i-2].label;
                    } else {
                        $scope.frames[i-2].gameTotal = $scope.frames[i-2].label + $scope.frames[i-3].gameTotal;
                    }

                }

                // controleer voor extra punten 1 frame terug
                if($scope.frames[i - 1].flings[0].strike){

                    $scope.frames[i - 1].label += $scope.frames[i].flings[0].total;
                    $scope.frames[i-1].gameTotal = $scope.frames[i-1].label + $scope.frames[i-1].gameTotal;
                }

                // Bij spare
                else if(true){
                    console.log('----------- meerdere worpen');
                    //$scope.frames[i - 1].label = $scope.frames[i - 1].label + $scope.frames[i].flings[0].total;
                        //$scope.frames[i - 2].label = $scope.frames[i - 2].label + $scope.frames[i - 1].flings[0].total;

                }


                $scope.frames[i].gameTotal = $scope.frames[i - 1].gameTotal + $scope.frames[i].total;
            }

        }
        else if($scope.frames.length == 10) {


        }
        else if($scope.frames.length == 2) {
            // Alleen bij strike
            if($scope.frames[0].flings[0].strike) {
                $scope.frames[0].label += $scope.frames[1].total;
                $scope.frames[0].gameTotal = $scope.frames[0].label;
            }

            // Alleen bij spare
            if(true ) {
                
            }

            $scope.frames[1].gameTotal += $scope.frames[1].total + $scope.frames[0].label;

        } else {
            $scope.frames[0].gameTotal = $scope.frames[0].total;
        }
        
        console.log('Dit is de data van Frame:', strikeFrame, $scope.frames[strikeFrame].flings[0].strike);
        console.log('deze beurt had ik ',$scope.frames[strikeFrame].flings[0].total, "kegels om.");
        
    }
    else {console.log('bestaat niet');} 

    //steek gegevens in database
    //postFrame($scope,game, frame, total);

    //var currentGame = $(".breadcrumbs .game").attr('data-game');
    //postFrame($scope, currentGame, $scope.frameNumber - 1, 50);
}

    function calcScore(currentScore){
        angular.forEach(flings, function(fling){
            console.log('flings', fling);
        });

        totalScore = totalScore + currentScore;

        return totalScore;
    }

    $scope.nextFling = function(){
        nextFling();
    }


    function saveFling(){

        //bij elke fling moet de score berekend worden
        score = 0;      
        pins = $scope.pins;
        hittedPins = {};

        angular.forEach(pins, function(pin){

            if (pin.selected) {
                hittedPins[pin.id] = true;

                var target = angular.element( document.querySelector('#checkbox' + [pin.id]));
                target.attr('disabled', true);

                score++;

            } else {
                //$scope.pinArray.push(false);
                hittedPins[pin.id] = false;
            }

        });
     
     

        thisScore = score;

        console.log('momenteel zijn we de ', $scope.flingNumber,'ste worp');
        // if(score == 10){
        //     if($scope.flingNumber == 1){
        //         $scope.labelvalue = 'X';
        //         $scope.isStrike = true;
        //         $scope.isSpare = false;
        //     }
        //     if($scope.flingNumber == 2){
        //         $scope.labelvalue = '/';
        //         $scope.isStrike = false; //behalve 10de beurt
        //         $scope.isSpare = true;
        //     }
        //     if($scope.flingNumber == 3){
        //         $scope.isStrike = true;
        //         $scope.isSpare = false;
        //     }
        // }



            //$scope.frames[number]flings[number]


        //     if(thisScore == 10){
        //       $scope.isStrike = true;
        //       if($scope.frameNumber == 10){ //als je een strike gooit in de laatste beurt

        //         console.log('strike', $scope.frameNumber);

        //         $scope.isLastFrame = true;

        //     }
        //     else {

        //     }
        // }

        console.log('scope.totalFling', $scope.totalFling);
        //console.log('jeroenwas here', totalFirst, totalSecond, totalThird);

        /*    if(thisScore > 0){
                console.log('test:::', $scope.total);
                    if(thisScore == 10){
                      if($scope.isStrike){

                        $scope.labelvalue = 'X';

                    }
                    else {
                        $scope.labelvalue = '/';
                    }

                }
                else {
                  $scope.labelvalue = thisScore;
                }
            }
            else {
                $scope.labelvalue = '-';
            }*/


            dataFling = {
                'id': $scope.flingNumber,
                'pins': hittedPins,
                'total': thisScore,
                'label': $scope.labelvalue,
                'strike': $scope.isStrike,
                'spare': $scope.isSpare
            };


        //hier vullen we hem
        flings = flings.concat(dataFling);
        resetPins();



        console.log('check hoeveel worpen', flings.length);
        if(flings.length == 1){
            console.log('check hoeveel kegels', flings[0].total);
            if(flings[0].total == 10){ //strike
                console.log('strike');
                setStrike();
                // saveFling();
                nextFrame();
            }
        }
        else if(flings[0].total + flings[1].total == 10) {
            console.log('spare 2de worp', flings[0].total);
            //spare
            setSpare();
            // saveFling();
            nextFrame();

            // if(flings[].total == 10){ //strike
            //     console.log('strike');
            //     setStrike();
            //     // saveFling();
            //     nextFrame();
            // }

        }
    }

    function checkLimit(variable, limit){

        if(variable < limit){ //als het binnen de limiet ligt
            console.log('true');
            return true;
        }
        else {
            console.log('false');
            return false;
        }
    }

    function nextFling(){

        if($scope.frameNumber < 10 && $scope.flingNumber <= 2) {

            saveFling();
            $scope.flingNumber++;
            
            angular.forEach(flings, function(fling){
                console.log('fling', fling);
            });

            if($scope.flingNumber == 3){
            //ga dan naar volgende beurt met worp 1
                nextFrame();
            }
        }
        // Handle round 10
        else if($scope.frameNumber == 10 && $scope.flingNumber <= 3) {
            saveFling();
            $scope.flingNumber++;
            
            angular.forEach(flings, function(fling){
                console.log('fling', fling);
            });

            if($scope.flingNumber == 4){
                console.log('end game');
            }
        }
     }
})

    .controller('SearchCtrl', function($scope) {
		
        $scope.results = [
                {id: 1, name: 'Sunset Bowling', city:'Kortrijk', postcalcode: 8500, country: 'Belgium' },
                {id: 2, name: 'Bowling Atlantis', city:'Ieper', postcalcode: 8900, country: 'Belgium' },
                {id: 3, name: 'Bowling paradiso', city:'Wervik', postcalcode: 8940, country: 'Belgium' },
                {id: 4, name: 'Bowling Kentucky 2000', city:'Wevelgem', postcalcode: 8930, country: 'Belgium' },
        ];
        //console.log("results:",$scope.results);
        $scope.getTotalResults = function(){
                return $scope.results.length;
        };

    })
        
    
    
    .controller('LocationCtrl', function($scope) {

        var options = {
          enableHighAccuracy: true,
          timeout: 5000,
          maximumAge: 0
        };

        function success(pos) {
          var crd = pos.coords;

          console.log('Your current position is:');
          console.log('Latitude : ' + crd.latitude);
          console.log('Longitude: ' + crd.longitude);
          console.log('More or less ' + crd.accuracy + ' meters.');

          $scope.latitude = crd.latitude;
          $scope.longitude = crd.longitude;
          
          getNames($scope.latitude, $scope.longitude);
          
            console.log(crd);
            
        };

        function error(err) {
          console.warn('ERROR(' + err.code + '): ' + err.message);
        };

        navigator.geolocation.getCurrentPosition(success, error, options);
        
        function getNames(lat, long){
            var request = new XMLHttpRequest();

            var method = 'GET';
            var url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + long + '&sensor=true';
            var async = true;

            request.open(method, url, async);
            request.onreadystatechange = function(){
                if(request.readyState == 4 && request.status == 200){

                    var data = JSON.parse(request.responseText);
                    var address = data.results[0];
                    console.log(address);
//                    $scope.location = {
//                      "street_number": address['address_components'][0]['long_name'],
//                      "route": address['address_components'][1]['long_name'],
//                      "city": address['address_components'][2]['long_name'],
//                      "county": address['address_components'][3]['long_name'],
//                      "region": address['address_components'][4]['long_name'],
//                      "country": address['address_components'][5]['long_name'],
//                      "postal_code": address['address_components'][6]['long_name'],
//                    };

                    $scope.streetNumber = address['address_components'][0]['long_name'];
                    $scope.route = address['address_components'][1]['long_name'];
                    $scope.city = address['address_components'][2]['long_name'];
                    $scope.county = address['address_components'][3]['long_name'];
                    $scope.region = address['address_components'][4]['long_name'];
                    $scope.country = address['address_components'][5]['long_name'];
                    $scope.postalCode = address['address_components'][6]['long_name'];
                    
                    $('.postalcode').val($scope.postalCode);
                    $('.city').val($scope.city);
                    $('.country').val($scope.country);
 
                    
                    //console.log($scope.location.route);
                //console.log('extra info: City: ' + locationCity + ', Postcode: ' + locationPostalCode + ', Country: ' + locationCountry);
                }
            };
            request.send();    
        }
    })
    
    .controller('BowlingCtrl', function($scope, $http) {
 
            $http({
              method: 'GET',
              url: 'http://www.bachelor.proef/api/v1/bowling.json?sort=id&order=desc'
              //url: './js/bowling.json'
            }).then(function successCallback(response) {
              // this callback will be called asynchronously
              // when the response is available
              //$scope.test = response.data.bowlingcentra;
              return response;
            }, function errorCallback(response) {
              // called asynchronously if an error occurs
              // or server returns response with an error status.
            }).then(function (response) {
              $scope.bowlingcentra = response.data.bowlingcentra;
            });
            

		$scope.getTotalResults = function(){
			return $scope.results.length;
		};
		

    })
    
    .controller('UserCtrl', function($scope, $http) {
 
            $http({
              method: 'GET',
              url: 'http://www.bachelor.proef/api/v1/users.json'
              //url: './js/bowling.json'
            }).then(function successCallback(response) {
              // this callback will be called asynchronously
              // when the response is available
              //$scope.test = response.data.bowlingcentra;
              return response;
            }, function errorCallback(response) {
              // called asynchronously if an error occurs
              // or server returns response with an error status.
            }).then(function (response) {
                console.log('response', response.data);
              $scope.users = response.data.users;
            });
            

            $scope.getTotalResults = function(){
                    return $scope.results.length;
            };
		

    });
