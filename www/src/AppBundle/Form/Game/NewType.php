<?php

namespace AppBundle\Form\Game;

use AppBundle\Entity\Game;
use AppBundle\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('serie', 'entity', array(
                'class' => 'AppBundle:Serie',
                'property' => 'name',
                'required' => false
            ))
            ->add('bowling', 'entity', array(
                'class' => 'AppBundle:Bowling',
                'property' => 'name',
                'required' => false
            ))
            ->add('total')
            ->add('type', 'entity', array(
                'class' => 'AppBundle:Type',
                'property' => 'name',
                'required' => false
            ))
            ->add('lane');
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Game::class,
        ]);
    }

    /**
     * Form name.
     *
     * @return string
     */
    public function getName()
    {
        return 'appbundle_game_new';
    }
}
