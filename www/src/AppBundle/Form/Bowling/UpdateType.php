<?php

namespace AppBundle\Form\Bowling;

use AppBundle\Entity\Bowling;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;

class UpdateType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('image', 'file', [
//                'data_class' => null,
//                'property_path' => 'image',
//                'invalid_message' => 'Please upload a valid image',
//                'required' => false,
//                'label' => 'Image'
//            ])
            ->add('name')
            ->add('lat')
            ->add('lng')
            ->add('status', 'choice', [
                'choices' => [
                    'active' => 'active',
                    'disabled' => 'disabled',
                    'pending' => 'pending',
                ],
                'choices_as_values' => true,
            ])
            ->add('postalcode')
            ->add('city')
            ->add('country')
            ->add('save', 'submit', array(
                        'attr' => array('class' => 'save')
                 ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Bowling::class,
        ]);
    }

    /**
     * Form name.
     *
     * @return string
     */
    public function getName()
    {
        return 'appbundle_bowling_update';
    }
}
