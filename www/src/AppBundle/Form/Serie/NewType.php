<?php

namespace AppBundle\Form\Serie;

use AppBundle\Entity\Serie;
use AppBundle\Entity\Type;
use AppBundle\Entity\Bowling;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;

class NewType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('type', 'entity', array(
                'class' => 'AppBundle:Type',
                'property' => 'name',
                'required' => false
            ))
            ->add('bowling', 'entity', array(
                'class' => 'AppBundle:Bowling',
                'property' => 'name',
                'required' => false
            ))
            //->add('pattern')
            ->add('lat')
            ->add('lng');
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Serie::class,
        ]);
    }

    /**
     * Form name.
     *
     * @return string
     */
    public function getName()
    {
        return 'appbundle_seriefront_new';
    }
}
