<?php

namespace AppBundle\Traits;

use Doctrine\ORM\Tools\Pagination\Paginator;

trait PaginatorTrait
{


    /**
     * @param $dql
     * @param $page
     * @param $rpp
     *
     * @return Paginator
     */
    public function paginate($dql, $page = 1, $rpp = 40)
    {
        $paginator = new Paginator($dql);

        $paginator->getQuery()
            ->setFirstResult($rpp * ($page - 1)) // Offset
            ->setMaxResults($rpp); // Limit

        return $paginator;
    }

    /**
     * @param $query
     * @param $page
     *
     * @return float
     */
    public function getPaginator($query, $page, $rpp)
    {
        $currentPage = $page;
        $totalcount = $query->count();
        $totalPages = ceil($totalcount / $rpp);

        return [
            'entityCount' => $totalcount,
            'currentPage' => $currentPage,
            'totalPages' => $totalPages,
            'rpp' => $rpp,
        ];
    }

    
    /**
     * @param $query
     * @param $page
     *
     * @return float
     */
    public function getCustomPaginator($query, $page, $rpp)
    {
        $currentPage = $page;
        $totalcount = count($query);
        $totalPages = ceil($totalcount / $rpp);

        return [
            'entityCount' => $totalcount,
            'currentPage' => $currentPage,
            'totalPages' => $totalPages,
            'rpp' => $rpp,
        ];
    }

}
