<?php

namespace AppBundle\Traits;

use AppBundle\Entity\User;
use AppBundle\Entity\Picture;
use AppBundle\Entity\Profile;

trait ImageTrait
{
    /**
     * @param User $user
     */
    public function saveImagePath(User $user)
    {
       if($file = $user->getImage()) {
           $uploadDirectory = 'uploads/avatars';
           $fileName = sha1_file($file->getRealPath()) . '.' . $file->guessExtension();
           $fileLocator = realpath($this->getParameter('kernel.root_dir') . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web') . DIRECTORY_SEPARATOR . $uploadDirectory;
           $file->move($fileLocator, $fileName);
           $user->setImagePath('/' . $uploadDirectory . '/' . $fileName);
       }
    }
    
    /**
     * @param Picture $picture
     */
    public function saveImage(Picture $picture)
    {
        if($file = $picture->getImage()) {
            $uploadDirectory = 'uploads/pictures';
            $fileName = sha1_file($file->getRealPath()) . '.' . $file->guessExtension();
            $fileLocator = realpath($this->getParameter('kernel.root_dir') . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web') . DIRECTORY_SEPARATOR . $uploadDirectory;
            $file->move($fileLocator, $fileName);
            $picture->setFilename('/' . $uploadDirectory . '/' . $fileName);
        }
    }
}
