<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Frame.
 *
 * @ORM\Table(name="frames")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\FrameRepository")
 */
class Frame
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
 
    /**
     *
     * @ORM\ManyToOne(targetEntity="Game", inversedBy="frames")
     * @ORM\JoinColumn(name="game_id", referencedColumnName="id")
     */
    protected $game;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="frame", type="integer", scale=2)
     */
    private $frame;
 
    /**
     * @var integer
     * 
     * @ORM\OneToMany(targetEntity="Fling", mappedBy="frame")
     */
    private $fling;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="total", type="integer", scale=3)
     */
    private $total;
    
    
    /**
     * Constructor.
     */
    public function __construct()
    {

    }
    
    
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    
    /**
     * Set Game
     *
     * @param $game
     *
     * @return Game
     */
    public function setGame($game)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get Game
     *
     * @return Game
     */
    public function getGame()
    {
        return $this->game;
    }
    
    /**
     * Set Frame
     *
     * @param $frame
     *
     * @return Frame
     */
    public function setFrame($frame)
    {
        $this->frame = $frame;

        return $this;
    }

    /**
     * Get frame
     *
     * @return Frame
     */
    public function getFrame()
    {
        return $this->frame;
    }

    /**
     * Set Fling
     *
     * @param $fling
     *
     * @return Fling
     */
    public function setFling($fling)
    {
        $this->fling = $fling;

        return $this;
    }

    /**
     * Get fling
     *
     * @return Fling
     */
    public function getFling()
    {
        return $this->fling;
    }
    
     /**
     * Set total
     *
     * @param string $total
     *
     * @return Total
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get Total
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    } 

}

