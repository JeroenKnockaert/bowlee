<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Game;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Serie.
 *
 * @ORM\Table(name="series")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\SerieRepository")
 */
class Serie
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
 
    /**
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    
    /**
     * @var integer
     * @JMS\Exclude()
     * @ORM\OneToMany(targetEntity="Game", mappedBy="serie")
     */
    private $games;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    protected $type; 
    
    /**
     * @var string
     * 
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
   
    /**
     *
     * @ORM\ManyToOne(targetEntity="Bowling")
     * @ORM\JoinColumn(name="bowling_id", referencedColumnName="id")
     */
    protected $bowling; 

    /**
     * @var string
     * 
     * @ORM\Column(name="pattern", type="string", length=255, nullable=true)
     */
    private $pattern;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="lat", type="decimal", scale=6)
     */
    private $lat;

    /**
     * @var integer
     *
     * @ORM\Column(name="lng", type="decimal", scale=6)
     */
    private $lng;

    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;
    
    
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
        $this->features = new ArrayCollection();

    }
    
    
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param $user
     *
     * @return Profile
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
    
     /**
     * Set name
     *
     * @param string $name
     *
     * @return Picture
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set games
     *
     * @param string $games
     *
     * @return Games
     */
    public function setGames($games)
    {
        $this->games = $games;

        return $this;
    }

    /**
     * Get games
     *
     * @return string
     */
    public function getGames()
    {
        return $this->games;
    }
    
    /**
     * Set bowling
     *
     * @param $bowling
     *
     * @return bowling
     */
    public function setBowling($bowling)
    {
        $this->bowling = $bowling;

        return $this;
    }

    /**
     * Get bowling
     *
     * @return Bowling
     */
    public function getBowling()
    {
        return $this->bowling;
    }  
   
    /**
     * Set pattern
     *
     * @param $pattern
     *
     * @return pattern
     */
    public function setPattern($pattern)
    {
        $this->pattern = $pattern;

        return $this;
    }

    /**
     * Get pattern
     *
     * @return Pattern
     */
    public function getPattern()
    {
        return $this->pattern;
    } 
    
    
    /**
     * Set type
     *
     * @param $type
     *
     * @return type
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return Type
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Set lat
     *
     * @param int $lat
     *
     * @return Picture
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return int
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lng
     *
     * @param integer $lng
     *
     * @return Picture
     */
    public function setLng($lng)
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * Get lng
     *
     * @return integer
     */
    public function getLng()
    {
        return $this->lng;
    }   
    
    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Picture
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

}

