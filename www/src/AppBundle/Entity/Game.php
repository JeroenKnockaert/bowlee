<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Serie;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Game.
 *
 * @ORM\Table(name="games")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\GameRepository")
 */
class Game
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Serie", inversedBy="games")
     * @ORM\JoinColumn(name="serie_id", referencedColumnName="id")
     */
    protected $serie;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Bowling")
     * @ORM\JoinColumn(name="bowling_id", referencedColumnName="id")
     */
    protected $bowling;     
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Type")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    protected $type; 
    
    /**
     * @var integer
     * @JMS\Exclude()
     * @ORM\OneToMany(targetEntity="Frame", mappedBy="game")
     * @ORM\OrderBy({"frame" = "ASC"})
     */
    private $frames;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="ball", type="string", length=255, nullable=true)
     */
    private $ball;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="note", type="string", length=255, nullable=true)
     */
    private $note;

    /**
     * @var integer
     * 
     * @ORM\Column(name="lane", type="integer", length=255, nullable=true)
     */
    private $lane;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="games")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="total", type="integer", scale=3)
     */
    private $total;
    
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());

    }
    
    
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bowling
     *
     * @param $bowling
     *
     * @return bowling
     */
    public function setBowling($bowling)
    {
        $this->bowling = $bowling;

        return $this;
    }

    /**
     * Get bowling
     *
     * @return Bowling
     */
    public function getBowling()
    {
        return $this->bowling;
    }
    
    /**
     * Set type
     *
     * @param $type
     *
     * @return type
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return Type
     */
    public function getType()
    {
        return $this->type;
    }
    
    
    /**
     * Set serie
     *
     * @param $serie
     *
     * @return Serie
     */
    public function setSerie($serie)
    {
        $this->serie = $serie;

        return $this;
    }

    /**
     * Get user
     *
     * @return Serie
     */
    public function getSerie()
    {
        return $this->serie;
    }

    /**
     * Set Frames
     *
     * @param $frames
     *
     * @return Frames
     */
    public function setFrames($frames)
    {
        $this->frames = $frames;

        return $this;
    }

    /**
     * Get frames
     *
     * @return Frames
     */
    public function getFrames()
    {
        return $this->frames;
    }
    
     /**
     * Set ball
     *
     * @param string $ball
     *
     * @return Ball
     */
    public function setBall($ball)
    {
        $this->ball = $ball;

        return $this;
    }

    /**
     * Get Ball
     *
     * @return string
     */
    public function getBall()
    {
        return $this->ball;
    }
    
    
    /**
     * Set note
     *
     * @param $note
     *
     * @return note
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return Note
     */
    public function getNote()
    {
        return $this->note;
    }  

    
    /**
     * Set lane
     *
     * @param $lane
     *
     * @return lane
     */
    public function setLane($lane)
    {
        $this->lane = $lane;

        return $this;
    }

    /**
     * Get lane
     *
     * @return Lane
     */
    public function getLane()
    {
        return $this->lane;
    } 
    
    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Picture
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set user
     *
     * @param $user
     *
     * @return User
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * Set total
     *
     * @param string $total
     *
     * @return Total
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get Total
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    } 
}

