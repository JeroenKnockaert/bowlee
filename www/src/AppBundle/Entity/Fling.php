<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Fling.
 *
 * @ORM\Table(name="flings")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\FlingRepository")
 */
class Fling
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
 
    /**
     * @var integer
     *
     * @ORM\Column(name="worp", type="integer", options={"unsigned":true})
     */
    private $worp;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Frame", inversedBy="flings")
     * @ORM\JoinColumn(name="frame_id", referencedColumnName="id")
     */
    protected $frame;
  
        /**
     * @var Boolean
     *
     * @ORM\Column(name="one", type="boolean", nullable=true)
     */
    private $one;

    /**
     * @var Boolean
     *
     * @ORM\Column(name="two", type="boolean", nullable=true)
     */
    private $two;

    /**
     * @var Boolean
     *
     * @ORM\Column(name="three", type="boolean", nullable=true)
     */
    private $three;
    
    /**
     * @var Boolean
     *
     * @ORM\Column(name="four", type="boolean", nullable=true)
     */
    private $four;
    
    /**
     * @var Boolean
     *
     * @ORM\Column(name="five", type="boolean", nullable=true)
     */
    private $five;

    /**
     * @var Boolean
     *
     * @ORM\Column(name="six", type="boolean", nullable=true)
     */
    private $six;
    
    /**
     * @var Boolean
     *
     * @ORM\Column(name="seven", type="boolean", nullable=true)
     */
    private $seven;
    
    /**
     * @var Boolean
     *
     * @ORM\Column(name="eight", type="boolean", nullable=true)
     */
    private $eight;
    
    /**
     * @var Boolean
     *
     * @ORM\Column(name="nine", type="boolean", nullable=true)
     */
    private $nine;

    
    /**
     * @var Boolean
     *
     * @ORM\Column(name="ten", type="boolean", nullable=true)
     */
    private $ten;

    /**
     * @var string
     *
     * @ORM\Column(name="score", type="string", options={"unsigned":true})
     */
    private $score;
    
    /**
     * @var Boolean
     *
     * @ORM\Column(name="is_split", type="boolean", nullable=true)
     */
    private $is_split;
    
    /**
     * @var Boolean
     *
     * @ORM\Column(name="is_foul", type="boolean", nullable=true)
     */
    private $is_foul;
    
    /**
     * @var Boolean
     *
     * @ORM\Column(name="is_spare", type="boolean", nullable=true)
     */
    private $is_spare;
    
    /**
     * @var Boolean
     *
     * @ORM\Column(name="is_strike", type="boolean", nullable=true)
     */
    private $is_strike;
    
    
    /**
     * Constructor.
     */
    public function __construct()
    {

    }
    
    
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Worp
     *
     * @param $worp
     *
     * @return Worp
     */
    public function setWorp($worp)
    {
        $this->worp = $worp;

        return $this;
    }

    /**
     * Get Worp
     *
     * @return Worp
     */
    public function getWorp()
    {
        return $this->worp;
    }
    
    /**
     * Set Frame
     *
     * @param $frame
     *
     * @return Frame
     */
    public function setFrame($frame)
    {
        $this->frame = $frame;

        return $this;
    }

    /**
     * Get frame
     *
     * @return Frame
     */
    public function getFrame()
    {
        return $this->frame;
    }

    /**
     * Set One
     *
     * @param $one
     *
     * @return One
     */
    public function setOne($one)
    {
        $this->one = $one;

        return $this;
    }

    /**
     * Get one
     *
     * @return One
     */
    public function getOne()
    {
        return $this->one;
    }

    
    /**
     * Set Two
     *
     * @param $two
     *
     * @return Two
     */
    public function setTwo($two)
    {
        $this->two = $two;

        return $this;
    }

    /**
     * Get two
     *
     * @return Two
     */
    public function getTwo()
    {
        return $this->two;
    }
    
    /**
     * Set Three
     *
     * @param $three
     *
     * @return Three
     */
    public function setThree($three)
    {
        $this->three = $three;

        return $this;
    }

    /**
     * Get three
     *
     * @return Three
     */
    public function getThree()
    {
        return $this->three;
    }    

    /**
     * Set Four
     *
     * @param $four
     *
     * @return Four
     */
    public function setFour($four)
    {
        $this->four = $four;

        return $this;
    }

    /**
     * Get four
     *
     * @return Four
     */
    public function getFour()
    {
        return $this->four;
    }
    
     /**
     * Set Five
     *
     * @param $five
     *
     * @return Five
     */
    public function setFive($five)
    {
        $this->five = $five;

        return $this;
    }

    /**
     * Get five
     *
     * @return Five
     */
    public function getFive()
    {
        return $this->five;
    }   
    
     /**
     * Set Six
     *
     * @param $six
     *
     * @return Six
     */
    public function setSix($six)
    {
        $this->six = $six;

        return $this;
    }

    /**
     * Get six
     *
     * @return Six
     */
    public function getSix()
    {
        return $this->six;
    }
    
    /**
     * Set Seven
     *
     * @param $seven
     *
     * @return Seven
     */
    public function setSeven($seven)
    {
        $this->seven = $seven;

        return $this;
    }

    /**
     * Get seven
     *
     * @return Seven
     */
    public function getSeven()
    {
        return $this->seven;
    }
    
    /**
     * Set Eight
     *
     * @param $eight
     *
     * @return Eight
     */
    public function setEight($eight)
    {
        $this->eight = $eight;

        return $this;
    }

    /**
     * Get eight
     *
     * @return Eight
     */
    public function getEight()
    {
        return $this->eight;
    }
    
    /**
     * Set Nine
     *
     * @param $nine
     *
     * @return Nine
     */
    public function setNine($nine)
    {
        $this->nine = $nine;

        return $this;
    }

    /**
     * Get nine
     *
     * @return Nine
     */
    public function getNine()
    {
        return $this->nine;
    }
    
    /**
     * Set Ten
     *
     * @param $ten
     *
     * @return Ten
     */
    public function setTen($ten)
    {
        $this->ten = $ten;

        return $this;
    }

    /**
     * Get ten
     *
     * @return Ten
     */
    public function getTen()
    {
        return $this->ten;
    }
    
    /**
     * Set Score
     *
     * @param $score
     *
     * @return Score
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get Score
     *
     * @return Score
     */
    public function getScore()
    {
        return $this->score;
    }
    
    /**
     * Set IsSplit
     *
     * @param $is_split
     *
     * @return IsSplit
     */
    public function setIsSplit($is_split)
    {
        $this->is_split = $is_split;

        return $this;
    }

    /**
     * Get is_split
     *
     * @return IsSplit
     */
    public function getIsSplit()
    {
        return $this->is_split;
    }
    
    /**
     * Set IsFoul
     *
     * @param $is_foul
     *
     * @return IsFoul
     */
    public function setIsFoul($is_foul)
    {
        $this->is_foul = $is_foul;

        return $this;
    }

    /**
     * Get is_foul
     *
     * @return IsFoul
     */
    public function getIsFoul()
    {
        return $this->is_foul;
    }

    /**
     * Set IsStrike
     *
     * @param $is_strike
     *
     * @return IsStrike
     */
    public function setIsStrike($is_strike)
    {
        $this->is_strike = $is_strike;

        return $this;
    }

    /**
     * Get is_strike
     *
     * @return IsStrike
     */
    public function getIsStrike()
    {
        return $this->is_strike;
    }
    
    /**
     * Set IsSpare
     *
     * @param $is_spare
     *
     * @return IsSpare
     */
    public function setIsSpare($is_spare)
    {
        $this->is_spare = $is_spare;

        return $this;
    }

    /**
     * Get is_spare
     *
     * @return IsSpare
     */
    public function getIsSpare()
    {
        return $this->is_spare;
    }
}

