<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use AppBundle\Traits\ContainerTrait;
use AppBundle\Traits\PasswordTrait;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadUserData.
 *
 * @author Jeroen Knockaert <jeroen.knockaert@student.arteveldehs.be>
 * @copyright Copyright © 2015-2016, Artevelde University College Ghent
 */
class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerTrait, PasswordTrait;

    const COUNT = 5;

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 2; // The order in which fixture(s) will be loaded.
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        $role = 'ROLE_ADMIN';
        
        $user = new User();
        $em->persist($user); // Manage Entity for persistence.
        $user
            ->setFirstName('Steve')
            ->setLastName('Jobs')
            ->setUsername('Admin026')
            ->setImagePath("/uploads/avatars/bowlee-default-1.png")
            ->setRoles(array('ROLE_ADMIN'))
            ->setStatus('active')
            ->setPasswordRaw('TestUser');
        $this->hashPassword($user);
        $this->addReference('administrator', $user); // Reference for the next Data Fixture(s).

        
        $user = new User();
        $em->persist($user); // Manage Entity for persistence.
        $user
            ->setFirstName('Jeroen')
            ->setLastName('Knockaert')
            ->setUsername('Jeroen026')
            ->setImagePath("https://scontent-bru2-1.xx.fbcdn.net/hprofile-xat1/v/t1.0-1/c0.0.160.160/p160x160/12043131_10207985751868061_2216641867792421264_n.jpg?oh=067a229a42966f7c10e852c253d31261&oe=5776D869")
            ->setPasswordRaw('TestUser')
            ->setRoles(array('ROLE_USER'))
            ->setStatus('active');
        $this->hashPassword($user);
        $this->addReference('TestUser', $user); // Reference for the next Data Fixture(s).

        
        for ($j = 0; $j < 35; ++$j) {
            $random = rand(1,14);
            
            $user = new User();
            $em->persist($user);
            $user
                ->setFirstName($faker->firstName())
                ->setLastName($faker->lastName())
                ->setUsername($faker->userName())
                ->setImagePath("/uploads/avatars/bowlee-default-".$random.".png")
                ->setPasswordRaw($faker->password())
                ->setRoles(array('ROLE_USER'))
                ->setStatus('active');
            $this->hashPassword($user);
            $this->addReference("TestUser-${j}", $user); // Reference for the next Data Fixture(s).
        }

        for ($k = 36; $k < 70; ++$k) {
            
            $random = rand(1,14);
            
            $user = new User();
            $em->persist($user);
            $user
                ->setFirstName($faker->firstName())
                ->setLastName($faker->lastName())
                ->setUsername($faker->userName())
                ->setImagePath("/uploads/avatars/bowlee-default-".$random.".png")
                ->setPasswordRaw($faker->password())
                ->setRoles(array('ROLE_USER'))
                ->setStatus('banned');
            $this->hashPassword($user);
            $this->addReference("TestUser-${k}", $user); // Reference for the next Data Fixture(s).
        }

        $em->flush(); // Persist all managed Entities.
    }
}
