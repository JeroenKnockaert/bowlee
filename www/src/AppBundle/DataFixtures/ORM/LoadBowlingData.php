<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Bowling;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;

/**
 * Class LoadBowlingData.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2015-2016, Artevelde University College Ghent
 */
class LoadBowlingData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 5; // The order in which fixture(s) will be loaded.
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $locale = 'nl_BE';
        $faker = Faker::create($locale);
        
        
        $bowling = new Bowling();
        $em->persist($bowling); // Manage Entity for persistence.
        $bowling
            ->setFileName($faker->imageUrl($width = 90, $height = 90))
            ->setHeader($faker->imageUrl($width = 400, $height = 150))
            ->setName("Sunset Bowling")
            ->setLat($faker->latitude)
            ->setLng($faker->longitude)
            ->setCity("Kortrijk")
            ->setPostalcode(8500)
            ->setCountry("Belgium")
            ->setStatus("active");
            $this->addReference('TestBowling', $bowling); // Reference for the next Data Fixture(s).
        
        for ($x = 0; $x < 25; ++$x) {

        $bowling = new Bowling();
        $em->persist($bowling); // Manage Entity for persistence.
        $bowling
            ->setFileName($faker->imageUrl($width = 90, $height = 90))
            ->setHeader($faker->imageUrl($width = 400, $height = 150))
            ->setName($faker->company." Bowling")
            ->setLat($faker->latitude)
            ->setLng($faker->longitude)
            ->setCity($faker->city)
            ->setPostalcode($faker->postcode)
            ->setCountry($faker->country)
            ->setStatus("pending");
            $this->addReference("TestBowling-${x}", $bowling);
            
        }
        
        for ($x = 25; $x < 50; ++$x) {

        $bowling = new Bowling();
        $em->persist($bowling); // Manage Entity for persistence.
        $bowling
            ->setFileName($faker->imageUrl($width = 90, $height = 90))
            ->setHeader($faker->imageUrl($width = 400, $height = 150))
            ->setName($faker->company." Bowling")
            ->setLat($faker->latitude)
            ->setLng($faker->longitude)
            ->setCity($faker->city)
            ->setPostalcode($faker->postcode)
            ->setCountry($faker->country)
            ->setStatus("active");
            $this->addReference("TestBowling-${x}", $bowling);
            
        }
        

        $em->flush(); // Persist all managed Entities.
    }
}
