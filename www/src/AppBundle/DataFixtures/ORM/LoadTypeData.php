<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Type;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;

/**
 * Class LoadTypeData.
 *
 * @author Jeroen Knockaert <jeroen.knockaert@student.arteveldehs.be>
 * @copyright Copyright © 2015-2016, Artevelde University College Ghent
 */
class LoadTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 6; // The order in which fixture(s) will be loaded.
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $locale = 'nl_BE';
        
        //Open Bowling
        $type = new Type();
        $em->persist($type);
        $type->setName("Open Bowling");
        $this->addReference('TestOpen', $type); // Reference for the next Data Fixture(s).
        
        //Score Training
        $type = new Type();
        $em->persist($type);
        $type->setName("Score Training");
        $this->addReference('TestScore', $type); // Reference for the next Data Fixture(s).
        
        //League
        $type = new Type();
        $em->persist($type);
        $type->setName("League");
        $this->addReference('TestLeague', $type); // Reference for the next Data Fixture(s).
        
        //Tournament
        $type = new Type();
        $em->persist($type);
        $type->setName("Tournament");
        $this->addReference('TestTournament', $type); // Reference for the next Data Fixture(s).


        $em->flush(); // Persist all managed Entities.
    }
}