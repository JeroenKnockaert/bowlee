<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Serie;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;

/**
 * Class LoadSerieData.
 *
 * @author Jeroen Knockaert <jeroen.knockaert@student.arteveldehs.be>
 * @copyright Copyright © 2015-2016, Artevelde University College Ghent
 */
class LoadSerieData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 7; // The order in which fixture(s) will be loaded.
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $locale = 'nl_BE';
        $faker = Faker::create($locale);
                
        //Serie
        $serie = new Serie();
        $em->persist($serie);
        $serie->setUser($this->getReference('TestUser-2'))
                ->setType($this->getReference('TestOpen'))
                ->setBowling($this->getReference('TestBowling'))
                ->setName("Verjaardag Louis")
                ->setPattern("")
                ->setLat($faker->latitude)
                ->setLng($faker->longitude);
        $this->addReference('TestSerie-0', $serie);
       
        
        for ($x = 1; $x < 30; ++$x) {

            $serie = new Serie();
            $em->persist($serie);
            $serie->setUser($this->getReference('TestUser'))
                    ->setType($this->getReference('TestTournament'))
                    ->setBowling($this->getReference('TestBowling'))
                    ->setName("BK Singles Regio 1")
                    ->setPattern("BK Tour Cheetah")
                    ->setLat($faker->latitude)
                    ->setLng($faker->longitude);
            $this->addReference("TestSerie-${x}", $serie);
            
            $x++;
            
            $serie = new Serie();
            $em->persist($serie);
            $serie->setUser($this->getReference('TestUser'))
                    ->setType($this->getReference('TestLeague'))
                    ->setBowling($this->getReference('TestBowling'))
                    ->setName("League 5")
                    ->setPattern("R - Stonehenge")
                    ->setLat($faker->latitude)
                    ->setLng($faker->longitude);
            $this->addReference("TestSerie-${x}", $serie);
            
            $x++;
            
            $serie = new Serie();
            $em->persist($serie);
            $serie->setUser($this->getReference("TestUser"))
                    ->setType($this->getReference('TestScore'))
                    ->setBowling($this->getReference('TestBowling'))
                    ->setName("Tournament #".$x)
                    ->setPattern("WINDING ROAD - 2839 (40 uL)")
                    ->setLat($faker->latitude)
                    ->setLng($faker->longitude);
            $this->addReference("TestSerie-${x}", $serie);

        }
        
        
        $em->flush(); // Persist all managed Entities.
    }
}
