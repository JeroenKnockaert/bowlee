<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Frame;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadFrameData.
 *
 * @author Jeroen Knockaert <jeroen.knockaert@student.arteveldehs.be>
 * @copyright Copyright © 2015-2016, Artevelde University College Ghent
 */
class LoadFrameData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 9; // The order in which fixture(s) will be loaded.
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $score = 15;
        for ($i = 1; $i < 10; ++$i) {
            $frame = new Frame();
            $em->persist($frame);
            $frame->setGame($this->getReference('TestGame'));
            $frame->setFrame($i);
            $frame->setTotal($score);
            $score = $score + 15;
            $this->addReference("TestFrame-{$i}", $frame);
        }
        
            $frame = new Frame();
            $em->persist($frame);
            $frame->setGame($this->getReference('TestGame'));
            $frame->setFrame($i);
            $score = 155;
            $frame->setTotal($score);
            $this->addReference("TestFrame-10", $frame);
            
//            $frame = new Frame();
//            $em->persist($frame);
//            $frame->setGame($this->getReference('TestGame'));
//            $frame->setFrame(10);
//            $frame->setTotal();
        
        $em->flush(); // Persist all managed Entities.
    }
}
