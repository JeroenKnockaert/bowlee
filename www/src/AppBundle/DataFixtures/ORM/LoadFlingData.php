<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Fling;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;

/**
 * Class LoadFlingData.
 *
 * @author Jeroen Knockaert <jeroen.knockaert@student.arteveldehs.be>
 * @copyright Copyright © 2015-2016, Artevelde University College Ghent
 */
class LoadFlingData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 10; // The order in which fixture(s) will be loaded.
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        
        for ($i = 1; $i < 11; ++$i) {
            $fling = new Fling();
            $em->persist($fling);
            $fling->setFrame($this->getReference("TestFrame-{$i}"));
            $fling->setWorp(1);
            $fling->setOne(true);
            $fling->setTwo(true);
            $fling->setThree(true);
            $fling->setFour(true);
            $fling->setFive(true);
            $fling->setSix(false);        
            $fling->setSeven(false);
            $fling->setEight(false);
            $fling->setNine(false);
            $fling->setTen(false);
            $fling->setScore(5);
            $fling->setIsSplit(false);
            $fling->setIsFoul(false);
            $fling->setIsSpare(false);
            $fling->setIsStrike(false);

            $fling = new Fling();
            $em->persist($fling);
            $fling->setFrame($this->getReference("TestFrame-{$i}"));
            $fling->setWorp(2);
            $fling->setOne(false);
            $fling->setTwo(false);
            $fling->setThree(false);
            $fling->setFour(false);
            $fling->setFive(false);
            $fling->setSix(true);        
            $fling->setSeven(true);
            $fling->setEight(true);
            $fling->setNine(true);
            $fling->setTen(true);
            $fling->setScore("/");
            $fling->setIsSplit(false);
            $fling->setIsFoul(false);
            $fling->setIsSpare(true);
            $fling->setIsStrike(false);
            
            
        }
        
            $fling = new Fling();
            $em->persist($fling);
            $fling->setFrame($this->getReference("TestFrame-10"));
            $fling->setWorp(3);
            $fling->setOne(true);
            $fling->setTwo(true);
            $fling->setThree(true);
            $fling->setFour(true);
            $fling->setFive(true);
            $fling->setSix(true);        
            $fling->setSeven(true);
            $fling->setEight(true);
            $fling->setNine(true);
            $fling->setTen(true);
            $fling->setScore("X");
            $fling->setIsSplit(false);
            $fling->setIsFoul(false);
            $fling->setIsSpare(false);
            $fling->setIsStrike(true);


        $em->flush(); // Persist all managed Entities.
    }
}
