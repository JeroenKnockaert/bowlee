<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Game;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadGameData.
 *
 * @author Jeroen Knockaert <jeroen.knockaert@student.arteveldehs.be>
 * @copyright Copyright © 2015-2016, Artevelde University College Ghent
 */
class LoadGameData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 8; // The order in which fixture(s) will be loaded.
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {

        
        $game = new Game();
        $em->persist($game);
        $game->setSerie($this->getReference('TestSerie-0'));
        $game->setBowling($this->getReference('TestBowling'));
        $game->setType($this->getReference('TestLeague'));
        $game->setBall("Brunswick Swarm");
        $game->setNote("This is a placeholder text, you should add notes here.");
        $game->setLane(1);
        $game->setUser($this->getReference('TestUser'));
        $game->setTotal(300);
        $this->addReference('TestGame', $game);
        
        $y= 1;
        for ($x = 1; $x < 11; ++$x) {
            for ($l = rand(1,5); $l < 7; ++$l) {
                $game = new Game();
                $em->persist($game);
                $game->setSerie($this->getReference("TestSerie-${y}"));
                $game->setBowling($this->getReference('TestBowling'));
                $game->setType($this->getReference('TestLeague'));
                $game->setBall("Roto Grip Wrecker");
                $game->setNote("This is a placeholder text, you should add notes here.");
                $game->setLane($y);
                $game->setUser($this->getReference('TestUser'));
                $game->setTotal(300);
            }
            $y++;
            for ($o = rand(2,5); $o < 7; ++$o) {
                $game = new Game();
                $em->persist($game);
                $game->setSerie($this->getReference("TestSerie-${y}"));
                $game->setBowling($this->getReference('TestBowling'));
                $game->setType($this->getReference('TestOpen'));
                $game->setBall("Storm IQ Tour " . $y);
                $game->setNote("This is a placeholder text, you should add notes here.");
                $game->setLane($y);
                $game->setUser($this->getReference("TestUser"));
                $game->setTotal(300);
            }
            $y++;
            for ($s = rand(1,6); $s < 7; ++$s) {
                $game = new Game();
                $em->persist($game);
                $game->setSerie($this->getReference("TestSerie-${y}"));
                $game->setBowling($this->getReference('TestBowling'));
                $game->setType($this->getReference('TestScore'));
                $game->setBall("Storm IQ Tour " . $y);
                $game->setNote("This is a placeholder text, you should add notes here.");
                $game->setLane($y);
                $game->setUser($this->getReference("TestUser"));
                $game->setTotal(300);
            }
            $y++;
        }

        $em->flush(); // Persist all managed Entities.
    }
}
