<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class DefaultController.
 *
 * @Route("")
 */
class DefaultController extends Controller
{
    /**
     * @Route("home", name="homepage")
     * @Template("default/index.html.twig")
     */
    public function indexAction()
    {
        //if user is logged in
        if( $this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') ){
            $authUser = $this->getUser();
                    
            $em = $this->getDoctrine()->getManager();

            $posts = $em->getRepository('AppBundle:PostAbstract')->findAll();

            $games = $em->getRepository('AppBundle:Game')->findLastGamesFromUser($authUser->getId(), 5);

            $series = $em->getRepository('AppBundle:Serie')->findLastSeriesFromUser($authUser->getId(), 5);

            return [
                'posts' => $posts,
                'games' => $games,
                'series' => $series

            ];
        } else { //if not logged in, redirect to login page
            return $this->redirect($this->generateUrl('security_login'));
        }

    }
}
