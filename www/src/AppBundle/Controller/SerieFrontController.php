<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Serie;
use AppBundle\Form\Serie as SerieForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Traits\PaginatorTrait;

/**
 * Class SerieFrontController.
 *
 * @Route("/serie")
 */
class SerieFrontController extends Controller
{

    /**
     * Displays a form to edit an existing Serie entity.
     *
     * @Route("/{id}/edit", name="frontserie_edit", requirements={
     *     "id": "\d+"
     * })
     * @Method("GET")
     * @Template("/Serie/edit.html.twig")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Serie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Serie entity.');
        }

        $editForm = $this->createEditForm($entity);

        //get map coords
        //$serieCoords = $em->getRepository('AppBundle:Serie')->getSerieCoords($id);
        
        
        return [
            //'serieCoords' => $serieCoords,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        ];
    }
    
    /**
     * Edits an existing Serie entity.
     *
     * @Route("/{id}", name="frontserie_update", requirements={
     *     "id": "\d+"
     * })
     * @Method("PUT")
     * @Template("/Serie/edit.html.twig")
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $serie = $em->getRepository('AppBundle:Serie')->find($id);

        if (!$serie) {
            throw $this->createNotFoundException('Unable to find Serie entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($serie);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            //$this->saveImage($serie);
            //$serie->setUpdatedAt(new \DateTime());

            $em->flush();

            return $this->redirect($this->generateUrl('serie_edit', array('id' => $id)));
        }
        

        return [
            'serie' => $serie,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }
    
    /**
     * Finds and displays a Serie entity.
     *
     * @Route("/{id}", name="seriefront_show", requirements={
     *     "id": "\d+"
     * })
     * @Method("GET")
     * @Template("/Serie/show.html.twig")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $serie = $em->getRepository('AppBundle:Serie')->find($id);

        if (!$serie) {
            throw $this->createNotFoundException('Unable to find Serie entity.');
        }
        //$emt = $this->getDoctrine()->getManager();
        //$user = $emt->getRepository('AppBundle:User')->find($serie->getUser()); //dirty id van serie

        $deleteForm = $this->createDeleteForm($id);
        $undeleteForm = $this->createUndeleteForm($id);
            
        //get map coords
        $serieCoords = $em->getRepository('AppBundle:Serie')->getSerieCoords($id);
        
//        $games = array();
//        foreach($serie->getGames() as $game) {
//         $games[] = $game;
//        }

        return [
            'serieCoords' => $serieCoords,
            'entity' => $serie,
            'delete_form' => $deleteForm->createView(),
            'undelete_form' => $undeleteForm->createView(),
        ];
    }

     /**
     * Deletes a Serie entity.
     *
     * @Route("/{id}", name="serie_delete", requirements={
     *     "id": "\d+"
     * })
     * @Method("DELETE")
     *
     * @param Request $request
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Serie')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }

            //$em->remove($entity);
            $entity->setLockedAt(new \DateTime());
            $entity->setUpdatedAt(new \DateTime());
            $em->flush();
        }

        return $this->redirect($this->generateUrl('serie_show', array('id' => $id)));
    }
    
        /**
     * Creates a form to delete a Serie entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('serie_delete', ['id' => $id]))
            ->setMethod('DELETE')
            ->add('submit', 'submit', [
                'label' => 'Lock Serie',
                'attr' => [
                'class' => 'btn btn-secondary'
               ]
            ])
            ->getForm()
            ;
    }
    
    /**
     * Undeletes a Serie entity.
     *
     * @Route("/_undelete={id}", name="serie_undelete", requirements={
     *     "id": "\d+"
     * })
     * @Method("PUT")
     * @param Request $request
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function undeleteAction(Request $request, $id)
    {
        $form = $this->createUndeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Serie')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Serie entity.');
            }


            $entity->setLockedAt(null);
            $entity->setUpdatedAt(new \DateTime());
            $em->flush();
        }

        return $this->redirect($this->generateUrl('serie_show', array('id' => $id)));
    }
    
    /**
     * Creates a form to undelete a User entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createUndeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('serie_undelete', ['id' => $id]))
            ->setMethod('PUT')
            ->add('submit', 'submit', [
                'label' => 'Unlock Serie',
                'attr' => [
                    'class' => 'btn btn-secondary'
                ]
            ])
            ->getForm()
            ;
    }
}
