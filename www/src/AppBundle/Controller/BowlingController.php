<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Bowling;
use AppBundle\Form\Bowling as BowlingForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Traits\PaginatorTrait;

/**
 * Class BowlingController.
 *
 * @Route("/admin/bowlingcentra")
 */
class BowlingController extends Controller
{
    use PaginatorTrait;
    
    /**
     * Lists all Bowling entities.
     *
     * @Route("/_page={page}_status={status}", name="bowlingcentra")
     * requirements={"page" = "\d+", "status" = "\D+"},
     * defaults={"page" = 1, "status" = all})
     * @Template("Admin/Bowling/index.html.twig")
     * @param int $page
     * @return array
     */
    public function indexAction($page = 1, $status = 'all')
    {
        
        //Results per page
        $rpp = 20;
        
        $em = $this->getDoctrine()->getManager();

        $bowlingcentra = $em->getRepository('AppBundle:Bowling')->getAllBowlings($page, $rpp, $status);
        $paginator = $this->getPaginator($bowlingcentra, $page, $rpp);
        
        $all = $em->getRepository('AppBundle:Bowling')->findAll();
        $active = $em->getRepository('AppBundle:Bowling')->findBy(array('status' => 'active'));
        $pending = $em->getRepository('AppBundle:Bowling')->findBy(array('status' => 'pending'));
        $disabled = $em->getRepository('AppBundle:Bowling')->findBy(array('status' => 'disabled'));
        
        return [
            'bowlingcentra' => $bowlingcentra,
            'all' => $all,
            'active' => $active,
            'pending' => $pending,
            'disabled' => $disabled,
            'paginator' => $paginator
        ];
    }

    /**
     * Creates a new Bowling entity.
     *
     * @Route("/", name="bowling_create")
     * @Method("POST")
     * @Template("Admin/Bowling/new.html.twig")
     *
     * @param Request $request
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction(Request $request)
    {
        $bowling = new Bowling();
        //$bowling->setUser($this->getUser());
        $form = $this->createCreateForm($bowling);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($bowling);
            $em->flush();

            return $this->redirect($this->generateUrl('bowlingcentra'));
        }

        return [
            'bowling' => $bowling,
            'new_form' => $form->createView(),
        ];
    }

    /**
     * Displays a form to edit an existing Bowling entity.
     *
     * @Route("/{id}/edit", name="bowling_edit", requirements={
     *     "id": "\d+"
     * })
     * @Method("GET")
     * @Template("Admin/Bowling/edit.html.twig")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Bowling')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bowling entity.');
        }

        $editForm = $this->createEditForm($entity);

        //get map coords
        $bowlingCoords = $em->getRepository('AppBundle:Bowling')->getBowlingCoords($id);
        
        
        return [
            'bowlingCoords' => $bowlingCoords,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        ];
    }
    
    /**
     * Creates a form to edit a Bowling entity.
     *
     * @param Bowling $bowling Bowling entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Bowling $bowling)
    {
        $formType = new BowlingForm\UpdateType();
        $form = $this->createForm($formType, $bowling, [
            'action' => $this->generateUrl('bowling_update', ['id' => $bowling->getId()]),
            'method' => 'PUT',
        ]);

        return $form;
    }
    
    /**
     * Edits an existing Bowling entity.
     *
     * @Route("/{id}", name="bowling_update", requirements={
     *     "id": "\d+"
     * })
     * @Method("PUT")
     * @Template("Admin/Bowling/edit.html.twig")
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $bowling = $em->getRepository('AppBundle:Bowling')->find($id);

        if (!$bowling) {
            throw $this->createNotFoundException('Unable to find Bowling entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($bowling);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            //$this->saveImage($bowling);
            //$bowling->setUpdatedAt(new \DateTime());

            $em->flush();

            return $this->redirect($this->generateUrl('bowling_edit', array('id' => $id)));
        }
        

        return [
            'bowling' => $bowling,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }
    
    /**
     * Finds and displays a Bowling entity.
     *
     * @Route("/{id}", name="bowling_show", requirements={
     *     "id": "\d+"
     * })
     * @Method("GET")
     * @Template("Admin/Bowling/show.html.twig")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $bowling = $em->getRepository('AppBundle:Bowling')->find($id);

        if (!$bowling) {
            throw $this->createNotFoundException('Unable to find Bowling entity.');
        }
        $emt = $this->getDoctrine()->getManager();
        //$user = $emt->getRepository('AppBundle:User')->find($bowling->getUser()); //dirty id van bowling

        $deleteForm = $this->createDeleteForm($id);
        $undeleteForm = $this->createUndeleteForm($id);
            
        //get map coords
        $bowlingCoords = $em->getRepository('AppBundle:Bowling')->getBowlingCoords($id);
        
        

        return [
            'bowlingCoords' => $bowlingCoords,
            'entity' => $bowling,
            'delete_form' => $deleteForm->createView(),
            'undelete_form' => $undeleteForm->createView(),
        ];
    }
    
    /**
     * Creates a form to create a Bowling entity.
     *
     * @param Bowling $bowling The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Bowling $bowling)
    {
        $formType = new BowlingForm\NewType();
        $form = $this->createForm($formType, $bowling, [
            'action' => $this->generateUrl('bowling_create'),
            'method' => Request::METHOD_POST,
        ]);

        return $form;
    }

    /**
     * Displays a form to create a new Bowling entity.
     *
     * @Route("/new", name="bowling_new")
     * @Method("GET")
     * @Template("Admin/Bowling/new.html.twig")
     */
    public function newAction()
    {
        $bowling = new Bowling();
        $newForm = $this->createCreateForm($bowling);

        return [
            'new_form' => $newForm->createView(),
        ];
    }
    
            /**
     * Deletes a Bowling entity.
     *
     * @Route("/{id}", name="bowling_delete", requirements={
     *     "id": "\d+"
     * })
     * @Method("DELETE")
     *
     * @param Request $request
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Bowling')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }

            //$em->remove($entity);
            $entity->setLockedAt(new \DateTime());
            $entity->setStatus("disabled");
            $entity->setUpdatedAt(new \DateTime());
            $em->flush();
        }

        return $this->redirect($this->generateUrl('bowling_show', array('id' => $id)));
    }
    
        /**
     * Creates a form to delete a Bowling entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bowling_delete', ['id' => $id]))
            ->setMethod('DELETE')
            ->add('submit', 'submit', [
                'label' => 'Disable Bowling',
                'attr' => [
                'class' => 'btn btn-secondary'
               ]
            ])
            ->getForm()
            ;
    }
    
    /**
     * Undeletes a Bowling entity.
     *
     * @Route("/_undelete={id}", name="bowling_undelete", requirements={
     *     "id": "\d+"
     * })
     * @Method("PUT")
     * @param Request $request
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function undeleteAction(Request $request, $id)
    {
        $form = $this->createUndeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Bowling')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Bowling entity.');
            }


            $entity->setLockedAt(null);
            $entity->setStatus("active");
            $entity->setUpdatedAt(new \DateTime());
            $em->flush();
        }

        return $this->redirect($this->generateUrl('bowling_show', array('id' => $id)));
    }
    
    /**
     * Creates a form to undelete a User entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createUndeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bowling_undelete', ['id' => $id]))
            ->setMethod('PUT')
            ->add('submit', 'submit', [
                'label' => 'Enable Bowling',
                'attr' => [
                    'class' => 'btn btn-secondary'
                ]
            ])
            ->getForm()
            ;
    }
}
