<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Game;
use AppBundle\Form\Game as GameForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Traits\PaginatorTrait;

/**
 * Class GameController.
 *
 * @Route("/game")
 */
class GameFrontController extends Controller
{
       
    /**
     * Lists all Game entities.
     *
     * @Route("/", name="gamefront_index")
     * @Method("GET")
     * @Template("Game/index.html.twig")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $games = $em->getRepository('AppBundle:Game')->findAll(array('sort' => 'id'), array('id' => 'ASC'));

//        dump($gamecentra); // Dump to the Symfony Development Toolbar.

        // Send variables to the view.
        return [
            'games' => $games,
        ];
    }
    
    /**
     * Finds and displays a Game entity.
     *
     * @Route("/{id}", name="gamefront_show",
     *  requirements={
     *     "id": "\d+"
     * })
     * @param int $id
     * @Template("Game/show.html_3.twig")
     * @return array
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        //Haal huidige game op
        $game = $em->getRepository('AppBundle:Game')->find($id);
        
        //Haal Serie van huidige game op
        //$serie = $em->getRepository('AppBundle:Serie')->find($game->getSerie());
        
        //Haal User van huidige game op
        $user = $em->getRepository('AppBundle:User')->find($game->getUser());
        
        if (!$game) {
            throw $this->createNotFoundException('Unable to find Game entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $undeleteForm = $this->createUndeleteForm($id);

        return [
            'user' => $user,
            'entity' => $game,
            'delete_form' => $deleteForm->createView(),
            'undelete_form' => $undeleteForm->createView(),
        ];
    }
    
    
    /**
     * Creates a new Game entity.
     *
     * @Route("/", name="gamefront_create")
     * @Method("POST")
     * @Template("Game/new.html.twig")
     *
     * @param Request $request
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction(Request $request)
    {
        $game = new Game();
        $game->setUser($this->getUser());
        $form = $this->createCreateForm($game);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($game);
            $em->flush();

            return $this->redirect($this->generateUrl('homepage'));
        }

        return [
            'game' => $game,
            'new_form' => $form->createView(),
        ];
    }

    /**
     * Displays a form to edit an existing Game entity.
     *
     * @Route("/{id}/edit", name="gamefront_edit", requirements={
     *     "id": "\d+"
     * })
     * @Method("GET")
     * @Template("Game/edit.html.twig")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Game')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Game entity.');
        }

        $editForm = $this->createEditForm($entity);

        //get map coords
        //$gameCoords = $em->getRepository('AppBundle:Game')->getGameCoords($id);
        
        
        return [
            //'gameCoords' => $gameCoords,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        ];
    }
    
    /**
     * Creates a form to edit a Game entity.
     *
     * @param Game $game Game entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Game $game)
    {
        $formType = new GameForm\UpdateType();
        $form = $this->createForm($formType, $game, [
            'action' => $this->generateUrl('gamefront_update', ['id' => $game->getId()]),
            'method' => 'PUT',
        ]);

        return $form;
    }
    
    /**
     * Edits an existing Game entity.
     *
     * @Route("/{id}/edit", name="gamefront_update", requirements={
     *     "id": "\d+"
     * })
     * @Method("PUT")
     * @Template("Game/edit.html.twig")
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $game = $em->getRepository('AppBundle:Game')->find($id);

        if (!$game) {
            throw $this->createNotFoundException('Unable to find Game entity.');
        }

        $editForm = $this->createEditForm($game);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            //$this->saveImage($game);
            //$game->setUpdatedAt(new \DateTime());

            $em->flush();

            return $this->redirect($this->generateUrl('gamefront_edit', array('id' => $id)));
        }
        

        return [
            'game' => $game,
            'edit_form' => $editForm->createView(),
        ];
    }
    
    /**
     * Creates a form to create a Game entity.
     *
     * @param Game $game The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Game $game)
    {
        $formType = new GameForm\NewType();
        $form = $this->createForm($formType, $game, [
            'action' => $this->generateUrl('gamefront_create'),
            'method' => Request::METHOD_POST,
        ]);

        return $form;
    }

    /**
     * Displays a form to create a new Game entity.
     *
     * @Route("/new", name="gamefront_new")
     * @Method("GET")
     * @Template("Game/new.html.twig")
     */
    public function newAction()
    {
        $game = new Game();
        $newForm = $this->createCreateForm($game);

        return [
            'new_form' => $newForm->createView(),
        ];
    }
    
            /**
     * Deletes a Game entity.
     *
     * @Route("/{id}", name="gamefront_delete", requirements={
     *     "id": "\d+"
     * })
     * @Method("DELETE")
     *
     * @param Request $request
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Game')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }

            //$em->remove($entity);
            $entity->setLockedAt(new \DateTime());
            $entity->setUpdatedAt(new \DateTime());
            $em->flush();
        }

        return $this->redirect($this->generateUrl('game_show', array('id' => $id)));
    }
    
        /**
     * Creates a form to delete a Game entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('game_delete', ['id' => $id]))
            ->setMethod('DELETE')
            ->add('submit', 'submit', [
                'label' => 'Lock Game',
                'attr' => [
                'class' => 'btn btn-secondary'
               ]
            ])
            ->getForm()
            ;
    }
    
    /**
     * Undeletes a Game entity.
     *
     * @Route("/_undelete={id}", name="gamefront_undelete", requirements={
     *     "id": "\d+"
     * })
     * @Method("PUT")
     * @param Request $request
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function undeleteAction(Request $request, $id)
    {
        $form = $this->createUndeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Game')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Game entity.');
            }


            $entity->setLockedAt(null);
            $entity->setUpdatedAt(new \DateTime());
            $em->flush();
        }

        return $this->redirect($this->generateUrl('game_show', array('id' => $id)));
    }
    
    /**
     * Creates a form to undelete a User entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createUndeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('game_undelete', ['id' => $id]))
            ->setMethod('PUT')
            ->add('submit', 'submit', [
                'label' => 'Unlock Game',
                'attr' => [
                    'class' => 'btn btn-secondary'
                ]
            ])
            ->getForm()
            ;
    }
}
