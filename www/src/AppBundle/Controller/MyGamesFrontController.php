<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Game;
use AppBundle\Form\Game as GameForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Traits\PaginatorTrait;

use Symfony\Component\DependencyInjection\Container;

/**
 * Class MyGamesFrontController.
 *
 * @Route("/games")
 */
class MyGamesFrontController extends Controller
{
     use PaginatorTrait;
       
    /**
     * Lists all Games from the current logged in user.
     *
     * @Route("/_page={page}_type={type}", name="gamesfront_index",
     *  requirements={
     *     "page": "\d+", "type": "\d+" },
     * defaults={"page" = 1, "type" = 5})
     * @param int $page
     * @param int $type
     * @param int $id
     * @Template("Games/index.html.twig")
     */
    public function indexAction($page = 1, $type = 5)
    {
        //Results per page
        $rpp = 12;
        
        $authUser = $this->getUser();
        $id = $authUser->getId();
        
        $em = $this->getDoctrine()->getManager();
        
        $user = $em->getRepository('AppBundle:User')->find($id);

        $games = $em->getRepository('AppBundle:Game')->findAllGamesFromUser($id, $page, $rpp, $type);
        
        $paginator = $this->getCustomPaginator($games, $page, $rpp);
        
//        dump($gamecentra); // Dump to the Symfony Development Toolbar.

        // Send variables to the view.
        return [
            'user' => $user,
            'type' => $type,
            'paginator' => $paginator,
            'games' => $games,
        ];
    }

    /**
     * Creates a new Game entity.
     *
     * @Route("/", name="gamefront_create")
     * @Method("POST")
     * @Template("Game/new.html.twig")
     *
     * @param Request $request
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction(Request $request)
    {
        $game = new Game();
        $game->setUser($this->getUser());
        $form = $this->createCreateForm($game);
        $form->handleRequest($request);

        $serie = $game->getSerie();
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($game);
            $em->flush();
            
            $em = $this->getDoctrine()->getManager();
            $game = $em->getRepository('AppBundle:Game')->findOneBy(
                    array('serie' => $serie->getId()),
                    array('id' => 'DESC')
            );
        
            //return $this->redirect($this->generateUrl('game_make', ['id' => $game->getId()]));
            //return $this->redirect($this->generateUrl('create_game', ['id' => $game->getId()]));
            
            return $this->redirect($this->generateUrl('games_create_view', ['game_id' => $game->getId()]));
        }

        return [
            'game' => $game,
            'new_form' => $form->createView(),
        ];
    }
    
    /**
     * Displays a form to create a new Serie entity.
     *
     * @Route("/new/{serie_id}/create", name="create_game",
     *  requirements = {
     *     "serie_id": "\d+"
     *  })
     * @param int $serie_id
     * @Method("GET")
     * @Template("Games/newgame.html.twig")
     */
    public function createGame($serie_id)
    {
        //hardgecodeerde value van user
        $id = 2;
        
        $em = $this->getDoctrine()->getManager();
        
        $user = $em->getRepository('AppBundle:User')->find($id);

        $now = new \DateTime();

        $game = new Game();
        $form = $this->createCreateForm($game);
        
        $serie = $em->getRepository('AppBundle:Serie')->find($serie_id);
        return [
            'serie' => $serie,
            'new_form' => $form->createView(),
        ];
    }

    /**
     * Displays a form to create a new Serie entity.
     *
     * @Route("/new", name="games_new")
     * @Method("GET")
     * @Template("Games/new.html.twig")
     */
    public function newGame()
    {
        //hardgecodeerde value van user
        $id = 2;
        
        $em = $this->getDoctrine()->getManager();
        
        $user = $em->getRepository('AppBundle:User')->find($id);

        $now = new \DateTime();



        $series = $em->getRepository('AppBundle:Serie')->getByDate($now, $id);
        return [
            'series' => $series,
        ];
    }
    
    
    /**
     * Displays a form to create a new Serie entity.
     *
     * @Route("/{game_id}/create", name="games_create_view",
     * requirements = {
     *     "game_id": "\d+"
     * })
     * @Method("GET")
     * @param $game_id
     * @Template("Games/create.html.twig")
     */
    public function createNewGame($game_id)
    {
        //hardgecodeerde value van user
        $id = 2;
        
        //$game = new Game();
        //$newForm = $this->createCreateForm($game);
        
        
        $em = $this->getDoctrine()->getManager();
        
        $user = $em->getRepository('AppBundle:User')->find($id);

        $game = $em->getRepository('AppBundle:Game')->find($game_id);
        
        return [
            'game' => $game,
            //'new_form' => $newForm->createView(),
            //'game' => $game,
        ];
    }
    
        /**
     * Creates a form to create a Game entity.
     *
     * @param Game $game The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Game $game)
    {
        $formType = new GameForm\NewType();
        $form = $this->createForm($formType, $game, [
            'action' => $this->generateUrl('gamefront_create'),
            'method' => Request::METHOD_POST,
        ]);

        return $form;
    }
}
