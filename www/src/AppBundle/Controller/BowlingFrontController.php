<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Bowling;
use AppBundle\Form\Bowling as BowlingForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Traits\PaginatorTrait;

/**
 * Class BowlingFrontController.
 *
 * @Route("/bowling")
 */
class BowlingFrontController extends Controller
{
    
    use PaginatorTrait;
    
    /**
     * Lists all Bowling entities.
     *
     * @Route("/_page={page}", name="bowlingfront",
     * requirements={"page" = "\d+"},
     * defaults={"page" = 1})
     * @Template("Bowling/index.html.twig")
     * @param int $page
     * @return array
     */
    public function indexAction($page = 1)
    {
        //Results per page
        $rpp = 10;
        $em = $this->getDoctrine()->getManager();
        $bowlingcentra = $em->getRepository('AppBundle:Bowling')->getAllBowlings($page, $rpp, 'active');
       
        
        $paginator = $this->getPaginator($bowlingcentra, $page, $rpp);
//        dump($bowlingcentra); // Dump to the Symfony Development Toolbar.

        // Send variables to the view.
        return [
            'bowlingcentra' => $bowlingcentra,
            'paginator' => $paginator
        ];
    }
    
    
    /**
     * Creates a new Bowling entity.
     *
     * @Route("/", name="bowlingfront_create")
     * @Method("POST")
     * @Template("Bowling/new.html.twig")
     *
     * @param Request $request
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction(Request $request)
    {
        $bowling = new Bowling();
        //$bowling->setUser($this->getUser());
        $form = $this->createCreateForm($bowling);
        $form->handleRequest($request);

        //$this = $bowling;
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($bowling);
            $em->flush();
            
            return $this->redirect($this->generateUrl('bowlingfront'));
            //return $this->redirect($this->generateUrl('bowlingfront_show', ['id' => $bowling->getId()]));
        }

        return [
            'bowling' => $bowling,
            'new_form' => $form->createView(),
        ];
    }

    /**
     * Displays a form to edit an existing Bowling entity.
     *
     * @Route("/{id}/edit", name="bowlingfront_edit", requirements={
     *     "id": "\d+"
     * })
     * @Method("GET")
     * @Template("Bowling/edit.html.twig")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Bowling')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bowling entity.');
        }

        $editForm = $this->createEditForm($entity);

        //get map coords
        $bowlingCoords = $em->getRepository('AppBundle:Bowling')->getBowlingCoords($id);
        
        
        return [
            'bowlingCoords' => $bowlingCoords,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        ];
    }
    
    /**
     * Creates a form to edit a Bowling entity.
     *
     * @param Bowling $bowling Bowling entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Bowling $bowling)
    {
        $formType = new BowlingForm\UpdateType();
        $form = $this->createForm($formType, $bowling, [
            'action' => $this->generateUrl('bowling_update', ['id' => $bowling->getId()]),
            'method' => 'PUT',
        ]);

        return $form;
    }
    
    /**
     * Edits an existing Bowling entity.
     *
     * @Route("/{id}", name="bowlingfront_update", requirements={
     *     "id": "\d+"
     * })
     * @Method("PUT")
     * @Template("Bowling/edit.html.twig")
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $bowling = $em->getRepository('AppBundle:Bowling')->find($id);

        if (!$bowling) {
            throw $this->createNotFoundException('Unable to find Bowling entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($bowling);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            //$this->saveImage($bowling);
            //$bowling->setUpdatedAt(new \DateTime());

            $em->flush();

            return $this->redirect($this->generateUrl('bowling_edit', array('id' => $id)));
        }
        

        return [
            'bowling' => $bowling,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }
    
    /**
     * Finds and displays a Bowling entity.
     *
     * @Route("/{id}/_page={page}_type={type}", name="bowlingfront_show",
     *  requirements={
     *     "id": "\d+", "page": "\d+", "type": "\d+" },
     * defaults={"page" = 1, "type" = 5})
     * @param int $page
     * @param int $type
     * @param int $id
     * @Template("Bowling/show.html.twig")
     * @return array
     */
    public function showAction($id, $page = 1, $type = 5)
    {
        //Results per page
        $rpp = 10;
        
        $em = $this->getDoctrine()->getManager();

        $bowling = $em->getRepository('AppBundle:Bowling')->find($id);

        $games = $em->getRepository('AppBundle:Game')->findAllGamesFromBowling($id, $page, $rpp, $type);

        $paginator = $this->getPaginator($games, $page, $rpp);
        
        //dump($user,$bowling);

        return [
            'entity' => $bowling,
            'filter_type' => $type,
            'paginator' => $paginator,
            'games' => $games,
        ];
    }
    
    
    /**
     * Creates a form to create a Bowling entity.
     *
     * @param Bowling $bowling The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Bowling $bowling)
    {
        $formType = new BowlingForm\NewType();
        $form = $this->createForm($formType, $bowling, [
            'action' => $this->generateUrl('bowlingfront_create'),
            'method' => Request::METHOD_POST,
        ]);

        return $form;
    }

    /**
     * Displays a form to create a new Bowling entity.
     *
     * @Route("/new", name="bowlingfront_new")
     * @Method("GET")
     * @Template("Bowling/new.html.twig")
     */
    public function newAction()
    {
        $bowling = new Bowling();
        $newForm = $this->createCreateForm($bowling);

        return [
            'new_form' => $newForm->createView(),
        ];
    }
    
            /**
     * Deletes a Bowling entity.
     *
     * @Route("/{id}", name="bowlingfront_delete", requirements={
     *     "id": "\d+"
     * })
     * @Method("DELETE")
     *
     * @param Request $request
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Bowling')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }

            //$em->remove($entity);
            $entity->setLockedAt(new \DateTime());
            $entity->setUpdatedAt(new \DateTime());
            $em->flush();
        }

        return $this->redirect($this->generateUrl('bowling_show', array('id' => $id)));
    }
    
        /**
     * Creates a form to delete a Bowling entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bowling_delete', ['id' => $id]))
            ->setMethod('DELETE')
            ->add('submit', 'submit', [
                'label' => 'Lock Bowling',
                'attr' => [
                'class' => 'btn btn-secondary'
               ]
            ])
            ->getForm()
            ;
    }
    
    /**
     * Undeletes a Bowling entity.
     *
     * @Route("/_undelete={id}", name="bowlingfront_undelete", requirements={
     *     "id": "\d+"
     * })
     * @Method("PUT")
     * @param Request $request
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function undeleteAction(Request $request, $id)
    {
        $form = $this->createUndeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Bowling')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Bowling entity.');
            }


            $entity->setLockedAt(null);
            $entity->setUpdatedAt(new \DateTime());
            $em->flush();
        }

        return $this->redirect($this->generateUrl('bowling_show', array('id' => $id)));
    }
    
    /**
     * Creates a form to undelete a User entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createUndeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bowling_undelete', ['id' => $id]))
            ->setMethod('PUT')
            ->add('submit', 'submit', [
                'label' => 'Unlock Bowling',
                'attr' => [
                    'class' => 'btn btn-secondary'
                ]
            ])
            ->getForm()
            ;
    }
    
        /**
     * Search Bowling by street, city or postal_code
     *
     * @Route("/_query={query}", name="bowling_search", requirements={"query" = ".*"}, defaults={"query" = null})
     * @Method("GET")
     * @return array
     */
    public function searchAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $bowlings = $em->getRepository('AppBundle:Bowling')->searchBowlings($request);


        $serializer = $this->container->get('serializer');
        $results = $serializer->serialize(['results'=> $bowlings], 'json');
        return new Response($results);
    }
}
