<?php

namespace AppBundle\Controller;


use AppBundle\Entity\User;
use AppBundle\Form\User as UserForm;
use AppBundle\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Traits\PasswordTrait;
use AppBundle\Traits\PaginatorTrait;
use AppBundle\Traits\ImageTrait;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;


/**
 * User controller.
 *
 * @Route("/user")
 */
class UserFrontController extends Controller
{
    use PasswordTrait;
    use ImageTrait;
    use PaginatorTrait;

    /**
     * Creates a new User entity.
     *
     * @Route("/new", name="usersfront_create")
     * @Method("POST")
     * @Template("User/new.html.twig")
     *
     * @param Request $request
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(new UserForm\NewType(), $user); // The form will be submitted to this action
        $form->handleRequest($request);
 
        if ($form->isSubmitted() && $form->isValid()) {
            $this->hashPassword($user); // From AppBundle\Traits\PasswordTrait
 
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
 
            // Depending on your entity class implementation, you may add your custom roles here
            //  backoffice_area is the name of the firewall you are using
            $token = new UsernamePasswordToken($user, null, 'backoffice_area', $user->getRoles());
            $this->get('security.context')->setToken($token);
            
            return $this->redirect($this->generateUrl('usersfront_show', ['id' => $user->getId()]));
        }
 
        return [
            'user' => $user,
            'new_form' => $form->createView(),
        ];
    }

    /**
     * Creates a form to create a User entity.
     *
     * @param User $user The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(User $user)
    {
        $formType = new UserForm\NewType();
        $form = $this->createForm($formType, $user, [
            'action' => $this->generateUrl('usersfront_create'),
            'method' => 'POST',
        ]);

        return $form;
    }

    /**
     * Displays a form to create a new User entity.
     *
     * @Route("/new", name="usersfront_new")
     * @Method("GET")
     * @Template("User/new.html.twig")
     */
    public function newAction()
    {
        $user = new User();
        $newForm = $this->createCreateForm($user);

        return [
            'new_form' => $newForm->createView(),
        ];
    }

    /**
     * Finds and displays a user entity.
     *
     * @Route("/{id}/_page={page}_type={type}", name="usersfront_show",
     *  requirements={
     *     "id": "\d+", "page": "\d+", "type": "\d+" },
     * defaults={"page" = 1, "type" = 5})
     * @param int $page
     * @param int $type
     * @param int $id
     * @Template("User/show.html.twig")
     * @return array
     */
    public function showAction($id, $page = 1, $type = 5)
    {
        //Results per page
        $rpp = 10;
        
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AppBundle:User')->find($id);
        
        $all = $em->getRepository('AppBundle:Game')->findBy(
                array('user' => $user->getId()),
                array('total' => 'DESC')
            );
        
        $games = $em->getRepository('AppBundle:Game')->findAllGamesFromUser($id, $page, $rpp, $type);
    
        $paginator = $this->getCustomPaginator($games, $page, $rpp);

        return [
            'entity' => $user,
            'all' => $all, 
            'type' => $type,
            'paginator' => $paginator,
            'games' => $games,
        ];
    }

    
    /**
     * Find games from a user
     *
     * @Route("/{id}/games/_page={page}_type={type}", name="user_games_index",
     * requirements={
     *     "id": "\d+", "page": "\d+", "type": "\d+"},
     * defaults = {"page" = 1, "type" = 5})
     * @param int $page
     * @param int $type
     * @param int $id
     * @Template("User/Games/index.html.twig")
     */
    public function getGamesFromUser($id, $page, $type)
    {
        //Results per page
        $rpp = 10;
        
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AppBundle:User')->find($id);
//        $games = $em->getRepository('AppBundle:Game')->findBy(array(
//            "user" => $user->getId())
//        );

        $games = $em->getRepository('AppBundle:Game')->findAllGamesFromUser($id, $page, $rpp, $type);
    
        $paginator = $this->getPaginator($games, $page, $rpp);
        
        return [
            'user' => $user,
            'games' => $games,
            'type' => $type,
            'paginator' => $paginator,
            //'delete_form' => $deleteForm->createView(),
            //'undelete_form' => $undeleteForm->createView(),
        ];
    }

    /**
     * Find a game from a user
     *
     * @Route("/{id}/game/{game_id}", name="user_games_show",
     * requirements={
     *     "id": "\d+", "game_id": "\d+"})
     * @param int $id
     * @param int $game_id
     * @Template("User/Games/show.html.twig")
     */
    public function userGameShowAction($id, $game_id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AppBundle:User')->find($id);
//        $games = $em->getRepository('AppBundle:Game')->findBy(array(
//            "user" => $user->getId())
//        );

        $games = $em->getRepository('AppBundle:Game')->find($game_id);
    
        //$paginator = $this->getPaginator($games, $page, $rpp);
        
        return [
            'user' => $user,
            'games' => $games,
            //'type' => $type,
            //'paginator' => $paginator,
            //'delete_form' => $deleteForm->createView(),
            //'undelete_form' => $undeleteForm->createView(),
        ];
    }
    
    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="usersfront_edit", requirements={
     *     "id": "\d+"
     * })
     * @Method("GET")
     * @Template("User/edit.html.twig")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createEditForm($entity);

        return [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            //'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Creates a form to edit a User entity.
     *
     * @param User $user User entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(User $user)
    {
        $formType = new UserType();
        $form = $this->createForm($formType, $user, [
            'action' => $this->generateUrl('usersfront_update', ['id' => $user->getId()]),
            'method' => 'PUT',
        ]);

        //$form->add('submit', 'submit', ['label' => 'Update']);

        return $form;
    }
    /**
     * Edits an existing User entity.
     *
     * @Route("/{id}", name="usersfront_update", requirements={
     *     "id": "\d+"
     * })
     * @Method("PUT")
     * @Template("User/show.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AppBundle:User')->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }


        $editForm = $this->createEditForm($user);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            
            $this->saveImagePath($user);
            //$user->setUpdatedAt(new \DateTime());
            
            $em->flush();

            return $this->redirect($this->generateUrl('usersfront_edit', array('id' => $id)));
        }

        //Results per page
        $rpp = 10;
        $type = 5;
        $page = 1;
        $games = $em->getRepository('AppBundle:Game')->findAllGamesFromUser($id, $page, $rpp, $type);
    
        $paginator = $this->getCustomPaginator($games, $page, $rpp);
        return [
            'user' => $user,
            'entity' => $user,
            'paginator' => $paginator,
            'type' => $type,
            'page' => $page, 
            'games' => $games,
            'edit_form' => $editForm->createView(),
//            'delete_form' => $deleteForm->createView(),
        ];
    }


}
