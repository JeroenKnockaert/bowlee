<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class AdminController extends Controller
{
    /**
     * @Route("/admin", name="admin")
     * @Template("Admin/index.html.twig")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $posts = $em->getRepository('AppBundle:PostAbstract')->findAll();
        $bowlingcentra = $em->getRepository('AppBundle:Bowling')->findAll();
        $users = $em->getRepository('AppBundle:User')->findAll();
        $series = $em->getRepository('AppBundle:Serie')->findAll();
        $games = $em->getRepository('AppBundle:Game')->findAll();

        $recentGames = $em->getRepository('AppBundle:Game')->findLastGames(12);
        $recentSeries = $em->getRepository('AppBundle:Serie')->findLastSeries(18);
       
        
        return [
            'posts' => $posts,
            'bowlingcentra' => $bowlingcentra,
            'users' => $users,
            'series' => $series,
            'games' => $games,
            'recentSeries' => $recentSeries,
            'recentGames' => $recentGames
        ];
    }
}
