<?php

namespace AppBundle\Controller;

use AppBundle\Traits\PasswordTrait;
use AppBundle\Entity\User;
use AppBundle\Form\User as UserForm;
use AppBundle\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Traits\PaginatorTrait;

/**
 * User controller.
 *
 * @Route("/admin/users")
 */
class UserController extends Controller
{
    use PaginatorTrait;
    use PasswordTrait;

    /**
     * Lists all User entities.
     *
     * @Route("/_page={page}_status={status}", name="users")
     * requirements={"page" = "\d+", "status" = "\D+"},
     * defaults={"page" = 1, "status" = all})
     * @Template("Admin/User/index.html.twig")
     * @param int $page
     * @return array
     */
    public function indexAction($page = 1, $status = 'all')
    {
        //Results per page
        $rpp = 20;
        
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:User')->getAllUsers($page, $rpp, $status);
        
        $paginator = $this->getPaginator($entities, $page, $rpp);
        
        $all = $em->getRepository('AppBundle:User')->findAll();
        $active = $em->getRepository('AppBundle:User')->findBy(array('status' => 'active'));
        $banned = $em->getRepository('AppBundle:User')->findBy(array('status' => 'banned'));

        

        return [
            'entities' => $entities,
            'all' => $all,
            'active' => $active,
            'banned' => $banned,
            'paginator' => $paginator
        ];
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/", name="users_create")
     * @Method("POST")
     * @Template("admin/User/new.html.twig")
     *
     * @param Request $request
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(new UserForm\NewType(), $user); // The form will be submitted to this action
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->hashPassword($user); // From AppBundle\Traits\PasswordTrait

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('users_show', ['id' => $user->getId()]));
        }

        return [
            'user' => $user,
            'new_form' => $form->createView(),
        ];
    }

    /**
     * Creates a form to create a User entity.
     *
     * @param User $user The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(User $user)
    {
        $formType = new UserForm\NewType();
        $form = $this->createForm($formType, $user, [
            'action' => $this->generateUrl('users_create'),
            'method' => 'POST',
        ]);

        return $form;
    }

    /**
     * Displays a form to create a new User entity.
     *
     * @Route("/new", name="users_new")
     * @Method("GET")
     * @Template("User/new.html.twig")
     */
    public function newAction()
    {
        $user = new User();
        $newForm = $this->createCreateForm($user);

        return [
            'new_form' => $newForm->createView(),
        ];
    }

    /**
     * Finds and displays a User entity.
     *
     * @Route("/{id}", name="users_show", requirements={
     *     "id": "\d+"
     * })
     * @Method("GET")
     * @Template("Admin/User/show.html.twig")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AppBundle:User')->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $undeleteForm = $this->createUndeleteForm($id);

        return [
            'entity' => $user,
            'delete_form' => $deleteForm->createView(),
            'undelete_form' => $undeleteForm->createView(),
        ];
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="users_edit", requirements={
     *     "id": "\d+"
     * })
     * @Method("GET")
     * @Template("Admin/User/edit.html.twig")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            //'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Creates a form to edit a User entity.
     *
     * @param User $user User entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(User $user)
    {
        $formType = new UserType();
        $form = $this->createForm($formType, $user, [
            'action' => $this->generateUrl('users_update', ['id' => $user->getId()]),
            'method' => 'PUT',
        ]);

        $form->add('submit', 'submit', ['label' => 'Update']);

        return $form;
    }
    /**
     * Edits an existing User entity.
     *
     * @Route("/{id}", name="users_update", requirements={
     *     "id": "\d+"
     * })
     * @Method("PUT")
     * @Template("Admin/User/show.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AppBundle:User')->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($user);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('users_edit', array('id' => $id)));
        }

        return [
            'user' => $user,
            'entity' => $user,
//            'edit_form' => $editForm->createView(),
//            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/{id}", name="users_delete", requirements={
     *     "id": "\d+"
     * })
     * @Method("DELETE")
     *
     * @param Request $request
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:User')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }

            $entity->setLockedAt(new \DateTime());
            $entity->setStatus("disabled");
            $entity->setUpdatedAt(new \DateTime());
            $em->flush();
        }

        return $this->redirect($this->generateUrl('users'));
    }

    /**
     * Creates a form to delete a User entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('users_delete', ['id' => $id]))
            ->setMethod('DELETE')
            ->add('submit', 'submit', [
                'label' => 'Disable User',
                'attr' => [
                'class' => 'btn btn-secondary'
               ]
            ])
            ->getForm()
            ;
    }
    
    /**
     * Undeletes a User entity.
     *
     * @Route("/_undelete={id}", name="user_undelete", requirements={
     *     "id": "\d+"
     * })
     * @Method("PUT")
     * @param Request $request
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function undeleteAction(Request $request, $id)
    {
        $form = $this->createUndeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:User')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }


            $entity->setLockedAt(null);
            $entity->setStatus("active");
            $entity->setUpdatedAt(new \DateTime());
            $em->flush();
        }

        return $this->redirect($this->generateUrl('users_show', array('id' => $id)));
    }
    
    /**
     * Creates a form to undelete a User entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createUndeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_undelete', ['id' => $id]))
            ->setMethod('PUT')
            ->add('submit', 'submit', [
                'label' => 'Enable User',
                'attr' => [
                    'class' => 'btn btn-secondary'
                ]
            ])
            ->getForm()
            ;
    }
}
