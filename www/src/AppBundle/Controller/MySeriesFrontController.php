<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Serie;
use AppBundle\Form\Serie as SerieForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Traits\PaginatorTrait;

/**
 * Class MySeriesFrontController.
 *
 * @Route("/series")
 */
class MySeriesFrontController extends Controller
{
    use PaginatorTrait;
    
    /**
     * Lists all Serie entities.
     *
     * @Route("/_page={page}_type={type}", name="seriesfront",
     * requirements = {
     *      "page": "\d+", "type": "\d+"},
     * defaults = { "page" = 1, "type" = 5})
     * @param int $page
     * @param int $type
     * @Template("Series/index.html.twig")
     */
    public function indexAction($page = 1, $type = 5)
    {  
        //Results per page
        $rpp = 12;
        
        $authUser = $this->getUser();
        $id = $authUser->getId();
        
        $em = $this->getDoctrine()->getManager();
        
        $user = $em->getRepository('AppBundle:User')->find($id);
        
        $series = $em->getRepository('AppBundle:Serie')->findAllSerieFromUser($id, $page, $rpp, $type);
        
        $paginator = $this->getCustomPaginator($series, $page, $rpp);
//        dump($seriecentra); // Dump to the Symfony Development Toolbar.

        // Send variables to the view.
        return [
            'user' => $user,
            'type' => $type,
            'paginator' => $paginator,
            'series' => $series,
        ];
    }

    /**
     * Creates a new Serie entity.
     *
     * @Route("/new", name="seriefront_create")
     * @Method("POST")
     * @Template("Series/index.html.twig")
     *
     * @param Request $request
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction(Request $request)
    {
        $serie = new Serie();
        $serie->setUser($this->getUser());
        $form = $this->createCreateForm($serie);
        $form->handleRequest($request);

        $globalSerie = $serie;
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($serie);
            $em->flush();
            
            $em = $this->getDoctrine()->getManager();
//            $globalSerie = $em->getRepository('AppBundle:Serie')->findOneBy(
//                    array('serie' => $serie->getId()),
//                    array('id' => 'DESC')
//            );
            
            return $this->redirect($this->generateUrl('create_game', ['serie_id' => $globalSerie->getId() ]));
        }
        
        $em = $this->getDoctrine()->getManager();
        
        $user = $em->getRepository('AppBundle:User')->find($this->getUser());
       
        //Results per page
        $rpp = 12;
        
        $page = 1;
        $type = 5;
        $id = $user->getId();  
        
        $series = $em->getRepository('AppBundle:Serie')->findAllSerieFromUser($id, $page, $rpp, $type); 
        
        $paginator = $this->getCustomPaginator($series, $page, $rpp); 
        
        $alert = [
            'message' => 'Something went wrong, An error has occurred with the Serie you tried to create.',
            'color' => 'danger'
        ];
        
        return [
            'serie' => $serie,
            'user' => $user,
            'entity' => $serie,
            'series' => $series,
            'type' => $type, 
            'paginator' => $paginator,
            'alert' => $alert,
            'new_form' => $form->createView(),
        ];
    }

    /**
     * Displays a form to edit an existing Serie entity.
     *
     * @Route("/{id}/edit", name="seriefront_edit", requirements={
     *     "id": "\d+"
     * })
     * @Method("GET")
     * @Template("Serie/edit.html.twig")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Serie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Serie entity.');
        }

        $editForm = $this->createEditForm($entity);

        //get map coords
        //$serieCoords = $em->getRepository('AppBundle:Serie')->getSerieCoords($id);
        
        
        return [
            //'serieCoords' => $serieCoords,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        ];
    }
    
    /**
     * Creates a form to edit a Serie entity.
     *
     * @param Serie $serie Serie entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Serie $serie)
    {
        $formType = new SerieForm\UpdateType();
        $form = $this->createForm($formType, $serie, [
            'action' => $this->generateUrl('serie_update', ['id' => $serie->getId()]),
            'method' => 'PUT',
        ]);

        return $form;
    }
    
    /**
     * Edits an existing Serie entity.
     *
     * @Route("/{id}", name="seriefront_update", requirements={
     *     "id": "\d+"
     * })
     * @Method("PUT")
     * @Template("Serie/edit.html.twig")
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $serie = $em->getRepository('AppBundle:Serie')->find($id);

        if (!$serie) {
            throw $this->createNotFoundException('Unable to find Serie entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($serie);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            //$this->saveImage($serie);
            //$serie->setUpdatedAt(new \DateTime());

            $em->flush();

            return $this->redirect($this->generateUrl('serie_edit', array('id' => $id)));
        }
        

        return [
            'serie' => $serie,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }

    /**
     * Creates a form to create a Serie entity.
     *
     * @param Serie $serie The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Serie $serie)
    {
        $formType = new SerieForm\NewType();
        $form = $this->createForm($formType, $serie, [
            'action' => $this->generateUrl('seriefront_create'),
            'method' => Request::METHOD_POST,
        ]);

        return $form;
    }

    /**
     * Displays a form to create a new Serie entity.
     *
     * @Route("/new", name="seriefront_type")
     * @Method("GET")
     * @Template("Series/type.html.twig")
     */
    public function newSerieType()
    {
        //$em = $this->getDoctrine()->getManager();
        //$type = $em->getRepository('AppBundle:Type')->findAll();
 
        return [
            //'type' => $type,
        ];
    }
    
    /**
     * Displays a form to create a new Serie entity.
     *
     * @Route("/new/{id}", name="seriefront_new",
     *  requirements = {
     *     "id": "\d+" })
     * @param int $id
     * @Method("GET")
     * @Template("Series/new.html.twig")
     */
    public function newAction($id)
    {
        $serie = new Serie();
        $newForm = $this->createCreateForm($serie);
        
        $em = $this->getDoctrine()->getManager();
        $type = $em->getRepository('AppBundle:Type')->find($id);
        
        $user = $em->getRepository('AppBundle:User')->find($this->getUser());
        
        return [
            'type' => $type,
            'user' => $user,
            'new_form' => $newForm->createView(),
        ];
    }
    
    /**
     * Deletes a Serie entity.
     *
     * @Route("/{id}", name="seriefront_delete", requirements={
     *     "id": "\d+"
     * })
     * @Method("DELETE")
     *
     * @param Request $request
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Serie')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }

            //$em->remove($entity);
            $entity->setLockedAt(new \DateTime());
            $entity->setUpdatedAt(new \DateTime());
            $em->flush();
        }

        return $this->redirect($this->generateUrl('serie_show', array('id' => $id)));
    }
    
        /**
     * Creates a form to delete a Serie entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('serie_delete', ['id' => $id]))
            ->setMethod('DELETE')
            ->add('submit', 'submit', [
                'label' => 'Lock Serie',
                'attr' => [
                'class' => 'btn btn-secondary'
               ]
            ])
            ->getForm()
            ;
    }
    
    /**
     * Undeletes a Serie entity.
     *
     * @Route("/_undelete={id}", name="seriefront_undelete", requirements={
     *     "id": "\d+"
     * })
     * @Method("PUT")
     * @param Request $request
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function undeleteAction(Request $request, $id)
    {
        $form = $this->createUndeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Serie')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Serie entity.');
            }


            $entity->setLockedAt(null);
            $entity->setUpdatedAt(new \DateTime());
            $em->flush();
        }

        return $this->redirect($this->generateUrl('serie_show', array('id' => $id)));
    }
    
    /**
     * Creates a form to undelete a User entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createUndeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('serie_undelete', ['id' => $id]))
            ->setMethod('PUT')
            ->add('submit', 'submit', [
                'label' => 'Unlock Serie',
                'attr' => [
                    'class' => 'btn btn-secondary'
                ]
            ])
            ->getForm()
            ;
    }
}
