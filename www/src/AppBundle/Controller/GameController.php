<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Game;
use AppBundle\Form\Game as GameForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Traits\PaginatorTrait;

/**
 * Class GameController.
 *
 * @Route("/admin/games")
 */
class GameController extends Controller
{
    use PaginatorTrait;
    
    
    /**
     * Lists all Serie entities.
     *
     * @Route("/_page={page}_type={type}", name="games")
     * requirements={"page" = "\d+","type" = "\d+"
     * defaults={"page" = 1)
     * @Template("Admin/Game/index.html.twig")
     * @param int $page
     * @return array
     */
    public function indexAction($page = 1, $type = 5)
    {
        //Results per page.
        $rpp = 20;
        $em = $this->getDoctrine()->getManager();

        $games = $em->getRepository('AppBundle:Game')->getAllGames($page, $rpp,$type);

        $paginator = $this->getPaginator($games, $page, $rpp);

        $all = $em->getRepository('AppBundle:Game')->findAll();
        $openBowling = $em->getRepository('AppBundle:Game')->findBy(array('type' => 1));
        $scoreTraining = $em->getRepository('AppBundle:Game')->findBy(array('type' => 2));
        $league = $em->getRepository('AppBundle:Game')->findBy(array('type' => 3));
        $tournament = $em->getRepository('AppBundle:Game')->findBy(array('type' => 4));
        
        return [
            'games' => $games,
            'paginator' => $paginator,
            'all' => $all, 
            'openBowling' => $openBowling,
            'scoreTraining' => $scoreTraining,
            'league' => $league,
            'tournament' => $tournament
        ];
    }


    /**
     * Displays a form to edit an existing Game entity.
     *
     * @Route("/{id}/edit", name="game_edit", requirements={
     *     "id": "\d+"
     * })
     * @Method("GET")
     * @Template("Admin/Game/edit.html.twig")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Game')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Game entity.');
        }

        $editForm = $this->createEditForm($entity);

        //get map coords
        //$gameCoords = $em->getRepository('AppBundle:Game')->getGameCoords($id);
        
        
        return [
            //'gameCoords' => $gameCoords,
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        ];
    }
    
    /**
     * Creates a form to edit a Game entity.
     *
     * @param Game $game Game entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Game $game)
    {
        $formType = new GameForm\UpdateType();
        $form = $this->createForm($formType, $game, [
            'action' => $this->generateUrl('game_update', ['id' => $game->getId()]),
            'method' => 'PUT',
        ]);

        return $form;
    }
    
    /**
     * Edits an existing Game entity.
     *
     * @Route("/{id}", name="game_update", requirements={
     *     "id": "\d+"
     * })
     * @Method("PUT")
     * @Template("Admin/Game/edit.html.twig")
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $game = $em->getRepository('AppBundle:Game')->find($id);

        if (!$game) {
            throw $this->createNotFoundException('Unable to find Game entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($game);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            //$this->saveImage($game);
            //$game->setUpdatedAt(new \DateTime());

            $em->flush();

            return $this->redirect($this->generateUrl('game_edit', array('id' => $id)));
        }
        

        return [
            'game' => $game,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ];
    }
    
    /**
     * Finds and displays a Game entity.
     *
     * @Route("/{id}", name="game_show", requirements={
     *     "id": "\d+"
     * })
     * @Method("GET")
     * @Template("Admin/Game/show.html.twig")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        
        //Haal huidige game op
        $game = $em->getRepository('AppBundle:Game')->find($id);
        
        //Haal Serie van huidige game op
        $serie = $em->getRepository('AppBundle:Serie')->find($game->getSerie());
        
        //Haal User van huidige game op
        $user = $em->getRepository('AppBundle:User')->find($game->getUser());
        
        if (!$game) {
            throw $this->createNotFoundException('Unable to find Game entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $undeleteForm = $this->createUndeleteForm($id);
            
              $series =  array(
                "id" => $serie->getId(),
                "games" => "",
                "type" => $serie->getType(),
                "name" => $serie->getName(),
                "bowling" => $serie->getBowling(),
                "pattern" => $serie->getPattern(),
                "lat" => $serie->getLat(),
                "lng" => $serie->getLng(),
                "createdAt" => $game->getCreatedAt()
            );

            $games =  array(
                "id" => $game->getId(),
                "frames" => "",
                "ball" => $game->getBall(),
                "note" => $game->getNote(),
                "createdAt" => $game->getCreatedAt(),
                "user" => array(
                    "id" => $user->getId(),
                    "firstName" => $user->getFirstName(),
                    "lastName" => $user->getLastName(),
                    "imagePath" => $user->getImagePath(),
                    "roles" => $user->getRoles(),
                    "username" => $user->getUsername(),
                    
                    ),
                "total" => $game->getTotal()
            );
        
        foreach ($game->getFrames() as $frames) {
            
            $counter = 0;
            
            $first = [];
            $second = [];
            $third = [];
            
            $scoreFirst = 0;
            $scoreSecond = 0;
            $scoreThird = 0;
            
            foreach ($frames->getFling() as $fling) {
                
                // Hier setten we de eerste worp
                if($fling->getWorp() == 1){
                    $scoreFirst = $fling->getScore();
                    $first = array(
                        "worp" => $fling->getWorp(),
                        "pins" => array(
                            "one" =>  $fling->getOne(),
                            "two" =>  $fling->getTwo(),
                            "three" =>  $fling->getThree(),
                            "four" =>  $fling->getFour(),
                            "five" =>  $fling->getFive(),
                            "six" =>  $fling->getSix(),
                            "seven" =>  $fling->getSeven(),
                            "eight" =>  $fling->getEight(),
                            "nine" =>  $fling->getNine(),
                            "ten" =>  $fling->getTen(),
                            "score" => $fling->getScore()
                        ),
                        "extra" => array(
                            "isSplit" =>  $fling->getIsSplit(),
                            "isFoul" =>  $fling->getIsFoul(),
                            "isSpare" =>  $fling->getIsSpare(),
                            "isStrike" =>  $fling->getIsStrike()
                        )
                    );
                }
                
                // Hier setten we de tweede worp
                if($fling->getWorp() == 2){
                    $scoreSecond = $fling->getScore();
                    $second = array(
                        "worp" => $fling->getWorp(),
                        "pins" => array(
                            "one" =>  $fling->getOne(),
                            "two" =>  $fling->getTwo(),
                            "three" =>  $fling->getThree(),
                            "four" =>  $fling->getFour(),
                            "five" =>  $fling->getFive(),
                            "six" =>  $fling->getSix(),
                            "seven" =>  $fling->getSeven(),
                            "eight" =>  $fling->getEight(),
                            "nine" =>  $fling->getNine(),
                            "ten" =>  $fling->getTen(),
                            "score" => $fling->getScore()
                        ),
                        "extra" => array(
                            "isSplit" =>  $fling->getIsSplit(),
                            "isFoul" =>  $fling->getIsFoul(),
                            "isSpare" =>  $fling->getIsSpare(),
                            "isStrike" =>  $fling->getIsStrike()
                        )
                    );
                }
                
                // Hier setten we de derde worp
                if($fling->getWorp() == 3){
                    $scoreThird = $fling->getScore();
                    $third = array(
                        "worp" => $fling->getWorp(),
                        "pins" => array(
                            "one" =>  $fling->getOne(),
                            "two" =>  $fling->getTwo(),
                            "three" =>  $fling->getThree(),
                            "four" =>  $fling->getFour(),
                            "five" =>  $fling->getFive(),
                            "six" =>  $fling->getSix(),
                            "seven" =>  $fling->getSeven(),
                            "eight" =>  $fling->getEight(),
                            "nine" =>  $fling->getNine(),
                            "ten" =>  $fling->getTen(),
                            "score" => $fling->getScore()
                        ),
                        "extra" => array(
                            "isSplit" =>  $fling->getIsSplit(),
                            "isFoul" =>  $fling->getIsFoul(),
                            "isSpare" =>  $fling->getIsSpare(),
                            "isStrike" =>  $fling->getIsStrike(),
                        )
                    );
                }
                

                    //flings
                    $flings = array(
                       "id" => $frames->getId(),
                        "frame" => $frames->getFrame(),
                            "fling" => array($first,$second, $third),
                        "first" => $scoreFirst,
                        "second" => $scoreSecond,
                        "third" => $scoreThird,
                        "total" => $frames->getTotal()
                    );
                $games["frames"][$frames->getId()] = $flings;


            }
        }

        $series["games"] = $games;
        
        return [
            'serie' => $series,
            'user' => $user,
            'entity' => $game,
            'delete_form' => $deleteForm->createView(),
            'undelete_form' => $undeleteForm->createView(),
        ];
    }
    
    /**
     * Creates a form to create a Game entity.
     *
     * @param Game $game The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Game $game)
    {
        $formType = new GameForm\NewType();
        $form = $this->createForm($formType, $game, [
            'action' => $this->generateUrl('game_create'),
            'method' => Request::METHOD_POST,
        ]);

        return $form;
    }

    /**
     * Displays a form to create a new Game entity.
     *
     * @Route("/new", name="game_new")
     * @Method("GET")
     * @Template("Admin/Game/new.html.twig")
     */
    public function newAction()
    {
        $game = new Game();
        $newForm = $this->createCreateForm($game);

        return [
            'new_form' => $newForm->createView(),
        ];
    }
    
            /**
     * Deletes a Game entity.
     *
     * @Route("/{id}", name="game_delete", requirements={
     *     "id": "\d+"
     * })
     * @Method("DELETE")
     *
     * @param Request $request
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Game')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }

            //$em->remove($entity);
            $entity->setLockedAt(new \DateTime());
            $entity->setUpdatedAt(new \DateTime());
            $em->flush();
        }

        return $this->redirect($this->generateUrl('game_show', array('id' => $id)));
    }
    
        /**
     * Creates a form to delete a Game entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('game_delete', ['id' => $id]))
            ->setMethod('DELETE')
            ->add('submit', 'submit', [
                'label' => 'Lock Game',
                'attr' => [
                'class' => 'btn btn-secondary'
               ]
            ])
            ->getForm()
            ;
    }
    
    /**
     * Undeletes a Game entity.
     *
     * @Route("/_undelete={id}", name="game_undelete", requirements={
     *     "id": "\d+"
     * })
     * @Method("PUT")
     * @param Request $request
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function undeleteAction(Request $request, $id)
    {
        $form = $this->createUndeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Game')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Game entity.');
            }


            $entity->setLockedAt(null);
            $entity->setUpdatedAt(new \DateTime());
            $em->flush();
        }

        return $this->redirect($this->generateUrl('game_show', array('id' => $id)));
    }
    
    /**
     * Creates a form to undelete a User entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createUndeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('game_undelete', ['id' => $id]))
            ->setMethod('PUT')
            ->add('submit', 'submit', [
                'label' => 'Unlock Game',
                'attr' => [
                    'class' => 'btn btn-secondary'
                ]
            ])
            ->getForm()
            ;
    }
}
