<?php

namespace ApiBundle\Form;

use AppBundle\Entity\Bowling;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BowlingType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('id', null, ['required' => false])
            ->add('name')
            ->add('filename')
            ->add('header')
            ->add('status')
            ->add('lat')
            ->add('lng')
            ->add('postalcode')
            ->add('city')
            ->add('country')
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Bowling::class,
            'csrf_protection' => false,
            'validation_groups' => [
                'Default',
            ],
        ]);
    }

    /**
     * JSON object name.
     *
     * { bowling: { … } }
     *
     * @return string
     */
    public function getName()
    {
        return 'bowling';
    }
}
