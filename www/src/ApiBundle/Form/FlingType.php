<?php

namespace ApiBundle\Form;

use AppBundle\Entity\Fling;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FlingType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('id', null, ['required' => false])
            //->add('id')
            //->add('id')
            ->add('frame')
            ->add('worp')
            ->add('one')
            ->add('two')
            ->add('three')
            ->add('four')
            ->add('five')
            ->add('six')
            ->add('seven')
            ->add('eight')
            ->add('nine')
            ->add('ten')
            ->add('score')
                //->add('label')
            ->add('isSplit')
            ->add('isFoul')
            ->add('isSpare')
            ->add('isStrike')
                
                
                
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Fling::class,
            'csrf_protection' => false,
            'validation_groups' => [
                'Default',
            ],
        ]);
    }

    /**
     * JSON object name.
     *
     * { fling: { … } }
     *
     * @return string
     */
    public function getName()
    {
        return 'fling';
    }
}
