<?php

namespace ApiBundle\Controller;

use ApiBundle\Form\FlingType;
use AppBundle\Entity\Fling;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\FOSRestController as Controller;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class FlingsController.
 */
class FlingsController extends Controller
{
    /**
     * Test API options and requirements.
     *
     * @return Response
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function optionsFramesAction()
    {
        $response = new Response();
        $response->headers->set('Allow', 'OPTIONS, GET, POST, PUT');

        return $response;
    }

    /**
     * @param $fling_id
     *
     * @return mixed
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function getFlingAction($fling_id)
    {
        $em = $this->getDoctrine()->getManager();
        $fling = $em->getRepository('AppBundle:Fling')->find($fling_id);

        if (!$fling instanceof Fling) {
            throw new NotFoundHttpException('Not found');
        }

        return $fling;
    }
    
    /**
     * @return mixed
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function getFlingsAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Fling');
        
        
        $fling  = $repository->findAll();

        return array('fling' => $fling);
    }
    
    /**
     * Delete a Fling.
     *
     * @param $fling_id
     *
     * @throws NotFoundHttpException
     * @FOSRest\View(statusCode = 204)
     * @FOSRest\Delete(
     *     requirements = {
     *         "fling_id"   : "\d+",
     *         "_format"   : "json|xml"
     *     },
     *     defaults = {"_format": "json"}
     * )
     * @Nelmio\ApiDoc(
     *     statusCodes = {
     *         Response::HTTP_NO_CONTENT: "No Content",
     *         Response::HTTP_NOT_FOUND : "Not Found"
     *     }
     * )
     */
    public function deleteFlingAction($fling_id)
    {
        $em = $this->getDoctrine()->getManager();

        $fling = $em
            ->getRepository('AppBundle:Fling')
            ->find($fling_id);

        if (!$fling instanceof Fling) {
            throw new NotFoundHttpException();
        }

        $em->remove($fling);
        $em->flush();
    }
    
    /**
     * Post a new fling.
     *
     * { "fling": { "id": 1, "game": 1, "frame": 1, total:150 } }
     *
     * @param Request $request
     * @param $id
     *
     * @return View|Response
     *
     * @FOSRest\View()
     * @FOSRest\Post(
     *     "/games/{game_id}/frames/{frame_id}/fling/{fling_nr}",
     *     requirements = {
     *         "game_id": "\d+",
     *         "frame_id": "\d+",
     *         "fling_nr": "\d+"
     *     }
     * )
     * @Nelmio\ApiDoc(
     *     input = FlingType::class,
     *     statusCodes = {
     *         Response::HTTP_CREATED : "Created"
     *     }
     * )
     */
    public function postFlingAction(Request $request, $game_id, $frame_id, $fling_nr)
    {
        $em = $this->getDoctrine()->getManager();

        //get game where id is game_id
        //$game = $em->getRepository('AppBundle:Game')->find($game_id);
        
        //get last frame from this game
        //$lastFrame = $em->getRepository('AppBundle:Frame')->findLastFrameFromGame($game->getId());
        
        //$lastFrame = 1;
//        
//        $frame = $em
//            ->getRepository('AppBundle:Frame')
//            ->find($lastFrame);
        
//        if (!$user instanceof User) {
//            throw new NotFoundHttpException();
//        }

        $fling = new Fling();
        $fling->setFrame($frame_id);

        $logger = $this->get('logger');
        $logger->info($request);

        return $this->processFlingForm($request, $fling);
    }
    
    // Convenience methods
    // -------------------

    /**
     * Process FlingType Form.
     *
     * @param Request $request
     * @param Fling $fling
     *
     * @return View|Response
     */
    private function processFlingForm(Request $request, Fling $fling)
    {
        $form = $this->createForm(new FlingType(), $fling, ['method' => $request->getMethod()]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $statusCode = is_null($fling->getId()) ? Response::HTTP_CREATED : Response::HTTP_NO_CONTENT;

            
            $em = $this->getDoctrine()->getManager();
            
            //$game = $em->getRepository('AppBundle:Game')->find($fling->getGame());
            
            $em->persist($fling); // Manage entity Article for persistence.
            $em->flush();           // Persist to database.

            $response = new Response();
            $response->setStatusCode($statusCode);

            // Redirect to the URI of the resource.
            $response->headers->set('Location',
                $this->generateUrl('gamefront_show', [
                    //'user_id' => $fling->getUser()->getId(),
                    'id' => 5,
                ], /* absolute path = */true)
            );
            $response->setContent(json_encode([
                    'fling' => ['id' => $fling->getId()],
                ]));

            return $response;
        }

        return View::create($form, Response::HTTP_BAD_REQUEST);
    }
}
