<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Bowling;
use ApiBundle\Form\BowlingType;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\FOSRestController as controller;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Traits\PaginatorTrait;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class BowlingsController.
 */
class BowlingsController extends Controller
{
    
    use PaginatorTrait;
        
    /**
     * Test API options and requirements.
     *
     * @return Response
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function optionsBowlingsAction()
    {
        $response = new Response();
        $response->headers->set('Allow', 'OPTIONS, GET, POST, PUT');

        return $response;
    }
    
    /**
     *
     * @param $bowling_id
     *
     * @return mixed
     * 
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK",
     *     }
     * )
     */
    public function getBowlingAction($bowling_id)
    {
        $em = $this->getDoctrine()->getManager();
        $bowling = $em->getRepository('AppBundle:Bowling')->find($bowling_id);

        if (!$bowling instanceof Bowling) {
            throw new NotFoundHttpException('Not found');
        }

        return $bowling;
    }
    
    
    /**
     * 
     * Return all bowlings.
     * 
     * @param ParamFetcher $params
     * @return mixed
     * @FOSRest\Get(
     *     "/bowling",
     *     requirements = {
     *          "_format": "json|jsonp|xml",
     *     }
     * )
     * @FOSRest\QueryParam(
     *     name = "sort",
     *     requirements = "id|name|city",
     *     default = "id",
     *     description = "Order by Bowling id, Bowling name or Bowling city"
     * )
     * @FOSRest\QueryParam(
     *     name = "order",
     *     requirements = "asc|desc",
     *     default = "desc",
     *     description = "Order result ascending or descending."
     * )
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function getBowlingsAction(ParamFetcher $params)
    {
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Bowling');
        
        //$bowlingcentra  = $repository->findAllBowlingCentra();       
        $bowlingcentra  = $repository->findAllBowlingCentra($params);
        //$bowlingcentra  = $repository->getAllBowlings($params);

        return array('bowlingcentra' => $bowlingcentra);
    }
    
    
    /**
     * Delete a Bowling.
     *
     * @param $bowling_id
     *
     * @throws NotFoundHttpException
     * @FOSRest\View(statusCode = 204)
     * @FOSRest\Delete(
     *     requirements = {
     *         "bowling_id"   : "\d+",
     *         "_format"   : "json|xml"
     *     },
     *     defaults = {"_format": "json"}
     * )
     * @Nelmio\ApiDoc(
     *     statusCodes = {
     *         Response::HTTP_NO_CONTENT: "No Content",
     *         Response::HTTP_NOT_FOUND : "Not Found"
     *     }
     * )
     */
    public function deleteBowlingAction($bowling_id)
    {
        $em = $this->getDoctrine()->getManager();

        $bowling = $em
            ->getRepository('AppBundle:Bowling')
            ->find($bowling_id);

        if (!$bowling instanceof Bowling) {
            throw new NotFoundHttpException();
        }

        $em->remove($bowling);
        $em->flush();
    }
    
    /**
     * Post a new bowling.
     *
     * { "bowling": { 
     *      "name": "Sunset Bowling",
     *      "filename": "http://www.pngall.com/wp-content/uploads/2016/03/Bowling-Free-Download-PNG.png",
     *      "status": "active",
     *      "lat": "ipsum",
     *      "lng": "ipsum",
     *      "postalcode": "8500",
     *      "city": "Kortrijk",
     *      "country": "Belgium"
     *      }
     *  }
     *
     * @param Request $request
     *
     * @return View|Response
     *
     * @FOSRest\View()
     * @FOSRest\Post(
     *     "/bowling",
     *     requirements = {
     *     }
     * )
     * @Nelmio\ApiDoc(
     *     input = BowlingType::class,
     *     statusCodes = {
     *         Response::HTTP_CREATED : "Created"
     *     }
     * )
     */
    public function postBowlingAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

//        $user = $em
//            ->getRepository('AppBundle:User')
//            ->find($user_id);
//        if (!$user instanceof User) {
//            throw new NotFoundHttpException();
//        }

        $bowling = new Bowling();
        //$bowling->setUser($user);

        $logger = $this->get('logger');
        $logger->info($request);

        return $this->processBowlingForm($request, $bowling);
    }
    
    // Convenience methods
    // -------------------

    /**
     * Process BowlingType Form.
     *
     * @param Request $request
     * @param Bowling $Bowling
     *
     * @return View|Response
     */
    private function processBowlingForm(Request $request, Bowling $bowling)
    {
        $form = $this->createForm(new BowlingType(), $bowling, ['method' => $request->getMethod()]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $statusCode = is_null($bowling->getId()) ? Response::HTTP_CREATED : Response::HTTP_NO_CONTENT;

            $em = $this->getDoctrine()->getManager();
            $em->persist($bowling); // Manage entity Bowling for persistence.
            $em->flush();           // Persist to database.

            $response = new Response();
            $response->setStatusCode($statusCode);

            // Redirect to the URI of the resource.
            $response->headers->set('Location',
                $this->generateUrl('bowlingfront', [
                    //'user_id' => $bowling->getUser()->getId(),
                    'bowling_id' => $bowling->getId(),
                ], /* absolute path = */true)
            );
            $response->setContent(json_encode([
                    'bowling' => ['id' => $bowling->getId()],
                ]));

            return $response;
        }

        return View::create($form, Response::HTTP_BAD_REQUEST);
    }
}
