<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Serie;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\FOSRestController as Controller;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class SeriesController.
 */
class SeriesController extends Controller
{
    /**
     * Test API options and requirements.
     *
     * @return Response
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function optionsSeriesAction()
    {
        $response = new Response();
        $response->headers->set('Allow', 'OPTIONS, GET, POST, PUT');

        return $response;
    }

    /**
     * @param $serie_id
     *
     * @return mixed
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function getSerieAction($serie_id)
    {
//        $em = $this->getDoctrine()->getManager();
//        $serie = $em->getRepository('AppBundle:Serie')->findOneSerie($serie_id);
        
         $serie = $this->getDoctrine()
                ->getRepository('AppBundle:Serie')                
                ->find($serie_id);

//        if (!$serie instanceof Serie) {
//            throw new NotFoundHttpException('Not found k');
//        }

        return array('serie' => $serie);
    }
    
    /**
     * @return mixed
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function getSeriesAction()
    {
//        $repository = $this->getDoctrine()
//            ->getRepository('AppBundle:Serie');
//
//        $serie = $repository->findAllSerieGames();

        
//        //DIT WERKT
//        $repository = $this->getDoctrine()->getRepository('AppBundle:Serie');
//
//        $query = $repository->createQueryBuilder('s')
//               ->select('s.id, s.name as name, s.lat, s.lng, s.createdAt, g.ball')
//                            ->join('s.games', 'g')
//                            //->where('g.id IN :game_ids')
//                            //->setParameter('game_ids', array(1, 3, 12))
//                            ->getQuery();
//
//        $result = $query->getResult();  
        
//        $repository = $this->getDoctrine()->getRepository('AppBundle:Serie');
//        
//        //$series = $repository->findAll();
//        
//        $query = $repository->createQueryBuilder('s')
//        ->select('s.id, s.name as name, s.lat, s.lng, s.createdAt')
//        ->orderby('s.id')
//        ->getQuery();
//
//      
//        $series = $query->getResult();  
        
//        $userArray = $user->toArray();
//        $result = json_encode($userArray);

        
        
//        $data = array(
//            'series' => $user->getId(),
//            'profile' => array(
//                'nickname' => $user->getProfile()->getNickname()
//            )
//        );
     
//        $serie = $this->getDoctrine()
//                     ->getRepository('AppBundle:Serie')                
//                     ->find(1);
        
        //$jeroen = $serie->getGames();
    
//        $data = array(
//            'series' => "lol",
//            'profile' => array(
//                'nickname' => $serie
//            )
//        );

        //return array('data' => $serie);

        
        $serie = $this->getDoctrine()
                ->getRepository('AppBundle:Serie')                
                ->findAll();

//        if (!$serie instanceof Serie) {
//            throw new NotFoundHttpException('Not found k');
//        }

        return array('serie' => $serie);
    }
  
    /**
     * Delete a Serie.
     *
     * @param $serie_id
     *
     * @throws NotFoundHttpException
     * @FOSRest\View(statusCode = 204)
     * @FOSRest\Delete(
     *     requirements = {
     *         "serie_id"   : "\d+",
     *         "_format"   : "json|xml"
     *     },
     *     defaults = {"_format": "json"}
     * )
     * @Nelmio\ApiDoc(
     *     statusCodes = {
     *         Response::HTTP_NO_CONTENT: "No Content",
     *         Response::HTTP_NOT_FOUND : "Not Found"
     *     }
     * )
     */
    public function deleteSerieAction($serie_id)
    {
        $em = $this->getDoctrine()->getManager();

        $serie = $em
            ->getRepository('AppBundle:Serie')
            ->find($serie_id);

        if (!$serie instanceof Serie) {
            throw new NotFoundHttpException();
        }

        $em->remove($serie);
        $em->flush();
    }
}
