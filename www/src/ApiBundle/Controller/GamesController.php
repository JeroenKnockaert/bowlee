<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Game;
use ApiBundle\Form\GameType;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\FOSRestController as controller;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class GamesController.
 */
class GamesController extends Controller
{
    /**
     * Test API options and requirements.
     *
     * @return Response
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function optionsGamesAction()
    {
        $response = new Response();
        $response->headers->set('Allow', 'OPTIONS, GET, POST, PUT');

        return $response;
    }

    /**
     *
     * @param $game_id
     *
     * @return mixed
     * 
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK",
     *     }
     * )
     */
    public function getGameAction($game_id)
    {
        $em = $this->getDoctrine()->getManager();
        $game = $em->getRepository('AppBundle:Game')->find($game_id);

        return $game;
    }

    /**
     *
     * @param $game_id
     *
     * @return mixed
     * 
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK",
     *     }
     * )
     */
    public function getGameFramesAction($game_id)
    {
        $em = $this->getDoctrine()->getManager();
        //$game = $em->getRepository('AppBundle:Game')->find($game_id);
        
        $frames = $em->getRepository('AppBundle:Frame')->findBy(
                array("game" => $game_id),
                array('frame' => 'ASC')
            );

        return $frames;
    }
    
    
    /**
     * 
     * Return all Games.
     * 
     * @param ParamFetcher $params
     * @return mixed
     * @FOSRest\Get(
     *     "/games",
     *     requirements = {
     *          "_format": "json|jsonp|xml",
     *     }
     * )
     * @FOSRest\QueryParam(
     *     name = "sort",
     *     requirements = "id",
     *     default = "id",
     *     description = "Order by game id"
     * )
     * @FOSRest\QueryParam(
     *     name = "order",
     *     requirements = "asc|desc",
     *     default = "desc",
     *     description = "Order result ascending or descending."
     * )
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function getGamesAction(ParamFetcher $params)
    {
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Game');
        
        
        //$game  = $repository->findAllGames();
        $games  = $repository->findAllGames($params);

        return $games;
    }
    
    /**
     * Returns all games from user.
     *
     * @param ParamFetcher $paramFetcher
     * @param $user_id
     *
     * @return mixed
     *
     * @FOSRest\View()
     * @FOSRest\Get(
     *     requirements = {
     *         "_format" : "json|jsonp|xml"
     *     }
     * )
     * @FOSRest\QueryParam(
     *     name = "sort",
     *     requirements = "id",
     *     default = "id",
     *     description = "Order by Game id."
     * )
     * @FOSRest\QueryParam(
     *     name = "order",
     *     requirements = "asc|desc",
     *     default = "asc",
     *     description = "Order result ascending or descending."
     * )
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK : "OK"
     *     }
     * )
     */
    public function getUserGamesAction(ParamFetcher $paramFetcher, $user_id)
    {
        # HTTP method: GET
        # Host/port  : http://www.nmdad3.arteveldehogeschool.local
        #
        # Path       : /app_dev.php/api/v1/users/1/articles.json
        # Path       : /app_dev.php/api/v1/users/1/articles.xml
        # Path       : /app_dev.php/api/v1/users/1/articles.xml?sort=title&amp;order=desc

//        dump([
//            $paramFetcher->get('sort'),
//            $paramFetcher->get('order'),
//            $paramFetcher->all(),
//        ]);

        $em = $this->getDoctrine()->getManager();
        $user = $em
            ->getRepository('AppBundle:User')
            ->find($user_id);

//        if (!$user instanceof User) {
//            throw new NotFoundHttpException('Not found');
//        }

        $games = $em->getRepository('AppBundle:Game')->findBy(array("user_id" => $user->getId()));


        return $games;
    }
    
    /**
     * Update a game.
     *
     * @param Request $request
     * @param $game_id
     *
     * @return Response
     *
     * @FOSRest\View()
     * @FOSRest\Put(
     *     requirements = {
     *         "game_id" : "\d+",
     *         "_format" : "json|xml"
     *     }
     * )
     * @Nelmio\ApiDoc(
     *     input = GameType::class,
     *     statusCodes = {
     *         Response::HTTP_NO_CONTENT: "No Content"
     *     }
     * )
     */
    public function putGameAction(Request $request, $game_id)
    {
        $em = $this->getDoctrine()->getManager();
        $game = $em
            ->getRepository('AppBundle:Game')->find($game_id);

       return $this->processGameForm($request, $game);
    }
    
    /**
     * Delete a Game.
     *
     * @param $game_id
     *
     * @throws NotFoundHttpException
     * @FOSRest\View(statusCode = 204)
     * @FOSRest\Delete(
     *     requirements = {
     *         "game_id"   : "\d+",
     *         "_format"   : "json|xml"
     *     },
     *     defaults = {"_format": "json"}
     * )
     * @Nelmio\ApiDoc(
     *     statusCodes = {
     *         Response::HTTP_NO_CONTENT: "No Content",
     *         Response::HTTP_NOT_FOUND : "Not Found"
     *     }
     * )
     */
    public function deleteGameAction($game_id)
    {
        $em = $this->getDoctrine()->getManager();

        $game = $em
            ->getRepository('AppBundle:Game')
            ->find($game_id);

        if (!$game instanceof Game) {
            throw new NotFoundHttpException();
        }

        $em->remove($game);
        $em->flush();
    }
    
    /**
     * Process GameType Form.
     *
     * @param Request $request
     * @param Game $game
     *
     * @return View|Response
     */
    private function processGameForm(Request $request, Game $game)
    {
        $form = $this->createForm(new GameType(), $game, ['method' => $request->getMethod()]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $statusCode = is_null($game->getId()) ? Response::HTTP_CREATED : Response::HTTP_NO_CONTENT;

            $em = $this->getDoctrine()->getManager();
            $em->persist($game); // Manage entity Game for persistence.
            $em->flush();           // Persist to database.

            $response = new Response();
            $response->setStatusCode($statusCode);

            // Redirect to the URI of the resource.
            $response->headers->set('Location',
                $this->generateUrl('gamefront_show', [
                    'id' => $game->getId(),
                ], /* absolute path = */true)
            );
            $response->setContent(json_encode([
                    'game' => ['id' => $game->getId()],
                ]));

            return $response;
        }

        return View::create($form, Response::HTTP_BAD_REQUEST);
    }
}
