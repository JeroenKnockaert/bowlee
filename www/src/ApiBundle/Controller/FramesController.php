<?php

namespace ApiBundle\Controller;

use ApiBundle\Form\FrameType;
use AppBundle\Entity\Frame;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\FOSRestController as Controller;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class FramesController.
 */
class FramesController extends Controller
{
    /**
     * Test API options and requirements.
     *
     * @return Response
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function optionsFramesAction()
    {
        $response = new Response();
        $response->headers->set('Allow', 'OPTIONS, GET, POST, PUT');

        return $response;
    }

    /**
     * @param $frame_id
     *
     * @return mixed
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function getFrameAction($frame_id)
    {
        $em = $this->getDoctrine()->getManager();
        $frame = $em->getRepository('AppBundle:Frame')->find($frame_id);

        if (!$frame instanceof Frame) {
            throw new NotFoundHttpException('Not found');
        }

        return $frame;
    }
    
    /**
     * @return mixed
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function getFramesAction()
    {
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Frame');
        
        
        $frame  = $repository->findAllFrames();

        return array('frame' => $frame);
    }
    
    /**
     * Delete a Frame.
     *
     * @param $frame_id
     *
     * @throws NotFoundHttpException
     * @FOSRest\View(statusCode = 204)
     * @FOSRest\Delete(
     *     requirements = {
     *         "frame_id"   : "\d+",
     *         "_format"   : "json|xml"
     *     },
     *     defaults = {"_format": "json"}
     * )
     * @Nelmio\ApiDoc(
     *     statusCodes = {
     *         Response::HTTP_NO_CONTENT: "No Content",
     *         Response::HTTP_NOT_FOUND : "Not Found"
     *     }
     * )
     */
    public function deleteFrameAction($frame_id)
    {
        $em = $this->getDoctrine()->getManager();

        $frame = $em
            ->getRepository('AppBundle:Frame')
            ->find($frame_id);

        if (!$frame instanceof Frame) {
            throw new NotFoundHttpException();
        }

        $em->remove($frame);
        $em->flush();
    }
    
    /**
     * Post a new frame.
     *
     * { "frame": { "id": 1, "game": 1, "frame": 1, total:150 } }
     *
     * @param Request $request
     * @param $id
     *
     * @return View|Response
     *
     * @FOSRest\View()
     * @FOSRest\Post(
     *     "/games/{id}/frames",
     *     requirements = {
     *         "id": "\d+"
     *     }
     * )
     * @Nelmio\ApiDoc(
     *     input = FrameType::class,
     *     statusCodes = {
     *         Response::HTTP_CREATED : "Created"
     *     }
     * )
     */
    public function postFrameAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $game = $em
            ->getRepository('AppBundle:Game')
            ->find($id);
//        if (!$user instanceof User) {
//            throw new NotFoundHttpException();
//        }

        $frame = new Frame();
        $frame->setGame($game);

        $logger = $this->get('logger');
        $logger->info($request);

        return $this->processFrameForm($request, $frame);
    }
    
    // Convenience methods
    // -------------------

    /**
     * Process FrameType Form.
     *
     * @param Request $request
     * @param Frame $frame
     *
     * @return View|Response
     */
    private function processFrameForm(Request $request, Frame $frame)
    {
        $form = $this->createForm(new FrameType(), $frame, ['method' => $request->getMethod()]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $statusCode = is_null($frame->getId()) ? Response::HTTP_CREATED : Response::HTTP_NO_CONTENT;

            
            $em = $this->getDoctrine()->getManager();
            
            $game = $em->getRepository('AppBundle:Game')->find($frame->getGame());
            
            $em->persist($frame); // Manage entity Article for persistence.
            $em->flush();           // Persist to database.

            $response = new Response();
            $response->setStatusCode($statusCode);

            // Redirect to the URI of the resource.
            $response->headers->set('Location',
                $this->generateUrl('gamefront_show', [
                    //'user_id' => $frame->getUser()->getId(),
                    'id' => $game->getId(),
                ], /* absolute path = */true)
            );
            $response->setContent(json_encode([
                    'frame' => ['id' => $frame->getId()],
                ]));

            return $response;
        }

        return View::create($form, Response::HTTP_BAD_REQUEST);
    }
}
